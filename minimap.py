import sys
import os
sys.path.insert(0, './staticmap/')
from staticmap import StaticMap, CircleMarker
import json


def download_osm_tiles(city_queried, json_file):

    # Load your json
    with open(json_file) as f:
        data = json.load(f)

    # Create your directory path
    directory_path = os.path.join('website', 'ville', city_queried, 'img')

    # If the directory does not exist, create it
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

    # Iterate over the data
    for key, value in data.items():
        lat = float(value['osm_lat'])
        lon = float(value['osm_lon'])
        id_osm = value['osm_id']
        type_osm = value['osm_type']

        filename = f'{directory_path}/{type_osm[:1]}{id_osm}.jpg'

        # Check if file exists and its size is more than 0.5kb
        if os.path.exists(filename) and os.path.getsize(filename) > 500:
            continue

        m = StaticMap(200, 200, url_template='http://a.tile.osm.org/{z}/{x}/{y}.png')

        marker_outline = CircleMarker((lon, lat), 'white', 18)
        marker = CircleMarker((lon, lat), '#0036FF', 12)

        m.add_marker(marker_outline)
        m.add_marker(marker)

        image = m.render(zoom=18)
        image.save(filename)


# Clean up old OSM tiles not needed anymore
def delete_unused_tiles(city_queried, json_file):
    with open(json_file) as f:
        data = json.load(f)

    # Create directory path and check it exists
    directory_path = os.path.join('website', 'ville', city_queried, 'img')
    if not os.path.exists(directory_path):
        print("Directory does not exist.")
        return

    # Create a set of all image filenames in the json file
    json_filenames = {f"{value['osm_type'][:1]}{value['osm_id']}.jpg" for value in data.values()}

    # Iterate over the files in the directory
    for filename in os.listdir(directory_path):
        # If the file is not in the json file, delete it
        if filename not in json_filenames:
            os.remove(os.path.join(directory_path, filename))


def process_osm_tiles(city_queried, json_file):
    download_osm_tiles(city_queried, json_file)
    delete_unused_tiles(city_queried, json_file)
