import sys
sys.path.insert(0, "staticmap")
from staticmap import StaticMap, CircleMarker

m = StaticMap(200, 200, url_template='http://a.tile.osm.org/{z}/{x}/{y}.png')

marker_outline = CircleMarker((7.2610268, 43.6997075), 'black', 15)
marker = CircleMarker((7.2610268, 43.6997075), '#0036FF', 9)

m.add_marker(marker_outline)
m.add_marker(marker)

image = m.render(zoom=18)
image.save('marker.jpg')
