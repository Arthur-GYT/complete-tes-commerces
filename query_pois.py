import overpass
from logger import logger


def build_overpass_query_pois_in_city(city, tags_dictionnary):
    # Building tags to be queried in overpass based on POI dictionnary
    # Get all relevant shops/amenities in city under .restos variable

    tags_query_with_names = ""
    tags_query_without_names = ""

    for poi_tag, value in tags_dictionnary.items():
        whitelist = "|".join(value["whitelist"])
        blacklist = "|".join(value["blacklist"])

        whitelist_string = f'["{poi_tag}"~"{whitelist}"]' if whitelist else ''
        blacklist_string = f'["{poi_tag}"!~"{blacklist}"]' if blacklist else ''

        tags_query_with_names += (f'nw["{poi_tag}"]{whitelist_string}{blacklist_string}'
                                  f'["name"](area.france)(area.ville);\n')
        tags_query_without_names += (f'nw["{poi_tag}"]{whitelist_string}{blacklist_string}'
                                     f'["name:signed"!~"no"]["name"!~".*"](area.france)(area.ville);\n')

    # Building complete overpass query
    query = f'''
area["admin_level"="2"]["name"="France"]->.france;
area["admin_level"="8"][name="{city}"]->.ville;
(
{tags_query_with_names});
out count;

(
{tags_query_without_names});
out count;
    '''

    return query


def build_overpass_query_pois_without_address(city, distance_poi, tags_dictionnary):
    # Building tags to be queried in overpass based on POI dictionnary
    # Get all relevant shops/amenities in city under .restos variable

    tags_query_string = "(\n"
    for poi_tag, value in tags_dictionnary.items():
        whitelist = "|".join(value["whitelist"])
        blacklist = "|".join(value["blacklist"])

        whitelist_string = f'["{poi_tag}"~"{whitelist}"]' if whitelist else ''
        blacklist_string = f'["{poi_tag}"!~"{blacklist}"]' if blacklist else ''

        tags_query_string += f'nw["{poi_tag}"]{whitelist_string}{blacklist_string}(area.france)(area.ville);\n'

    tags_query_string += ")->.restos;"

    # Building complete overpass query
    query = f'''
[out:json][timeout:300];
area["admin_level"="2"]["name"="France"]->.france;
area["admin_level"="8"]["name"={city}]->.ville;
nw["addr:street"](area.france)(area.ville)->.address;
way["highway"](area.france)(area.ville)->.routes;

{tags_query_string}

// From these city's shops/amenities, get only ones without an address defined and a name signage
( 
  nw.restos["contact:street"!~"."]["addr:street"!~"."]["addr:place"!~"."]["name:signed"!~"no"];
  nw.restos["contact:housenumber"!~"."]["addr:housenumber"!~"."]["addr:place"!~"."]["name:signed"!~"no"];
)->.restos_without_address;

// Select all provides_feature relations, where one of the shop/amenity is a member
// Then for thoses relations, find all related node members
// Then same for way members (building areas)
rel["type"="provides_feature"](bn.restos_without_address);
node(r)->.node_restos_in_relation_prov;
rel["type"="provides_feature"](bw.restos_without_address);
way(r)->.way_restos_in_relation_prov;

// Same for associatedStreet relations
rel["type"="associatedStreet"](bn.restos_without_address);
node(r)->.node_restos_in_relation_assoc;
rel["type"="associatedStreet"](bw.restos_without_address);
way(r)->.way_restos_in_relation_assoc;

// Combine both node/way and get the ones not part of this relation
( .node_restos_in_relation_prov; .way_restos_in_relation_prov; .node_restos_in_relation_assoc; 
.way_restos_in_relation_assoc;)->.restos_in_relation;
( .restos_without_address; - .restos_in_relation;);

// Loop to get addresses/streets around each specific shop/amenity
foreach->.remaining_shop_amenities
(
  // Check around POI for shops/amenities' addresses as well as streets
  nw.address(around.remaining_shop_amenities:{distance_poi})->.potential_housenum;
  way.routes(around.remaining_shop_amenities:{distance_poi})->.potential_streets;

  make Feature
    osm_id = remaining_shop_amenities.u(id()),
    osm_type = remaining_shop_amenities.u(type()),
    osm_lat = remaining_shop_amenities.u(lat()),
    osm_lon = remaining_shop_amenities.u(lon()),
    name = remaining_shop_amenities.u(t["name"]),
    shop = remaining_shop_amenities.u(t["shop"]),
    amenity = remaining_shop_amenities.u(t["amenity"]),
    siret = remaining_shop_amenities.u(t["ref:FR:SIRET"]),
    addrtag = remaining_shop_amenities.u(t["addr:street"] + t["contact:street"]),
    housetag = remaining_shop_amenities.u(t["addr:housenumber"] + t["contact:housenumber"]),
    website = remaining_shop_amenities.u(t["website"] + "," + t["facebook"] + "," + t["instagram"]),
    contactwebsite = remaining_shop_amenities.u(t["contact:website"] + "," + t["contact:facebook"] + "," + t["contact:instagram"]),
    potential_streets = potential_streets.set(t['name']),
    potential_addresses = potential_housenum.set(t["addr:housenumber"] + "|" + t["addr:street"]),
    url = "https://www.osm.org/" + remaining_shop_amenities.u(type()) + "/" + remaining_shop_amenities.u(id());
  out;
  )
    '''

    logger.info("####################################################################")
    logger.info("########################## Overpass Query ##########################")
    logger.info("####################################################################")
    logger.info(query)
    logger.info("\n#################################################################")
    logger.info("######################## Sirene DB Debug ########################")
    logger.info("#################################################################")

    return query


def get_overpass_response(city_queried, distance_to_poi, dict_osm_pois_tags):
    query_overpass = build_overpass_query_pois_without_address(city_queried, distance_to_poi, dict_osm_pois_tags)

    # Execute overpass query
    api = overpass.API(timeout=300)
    response = api.Get(query_overpass, responseformat=f"json", verbosity='geom', build=False)

    return response['elements']


def get_count_total_pois_city(city_queried, dict_osm_pois_tags):
    query_overpass = build_overpass_query_pois_in_city(city_queried, dict_osm_pois_tags)

    # Execute overpass query
    api = overpass.API(timeout=60)
    response = api.Get(query_overpass, responseformat=f"json", verbosity='count')

    # Extract the total count from the response (should contain 2 count elements, one named and one unnamed POIs)
    counts = {'named': 0, 'unnamed': 0}

    # Extract the total count from the response
    if response and "elements" in response:  # Check if there are any elements in the response
        for element in response["elements"]:
            if element["type"] == "count":
                if "tags" in element and "total" in element["tags"]:
                    # Assuming the first count element is for named and second is for unnamed POIs
                    if counts['named'] == 0:
                        counts['named'] = int(element["tags"]["total"])
                    else:
                        counts['unnamed'] = int(element["tags"]["total"])
                else:
                    raise ValueError("An element does not contain the expected 'tags' with 'total'.")
            else:
                raise ValueError("An element is not of type 'count'.")
    else:
        raise ValueError("No elements in the response or malformed response.")

    return counts
