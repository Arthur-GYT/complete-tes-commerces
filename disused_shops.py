from pre_processing import get_insee_code_from_city_name, get_city_name_from_insee_code
from frontend import create_bulma_theme
import overpass
import datetime
import json
import os
from dotenv import load_dotenv
from sklearn.cluster import DBSCAN
from haversine import haversine, Unit
import numpy as np
import folium
from folium.plugins import LocateControl
import re
import sys

# Get environment variables
load_dotenv()
sirene_api_key = os.getenv('SIRENE_API_KEY')
sirene_api_secret = os.getenv('SIRENE_API_SECRET')


def build_overpass_query_pois_with_siret(city, tags_dictionnary):

    tags_query_string = "(\n"
    for poi_tag, value in tags_dictionnary.items():
        whitelist = "|".join(value["whitelist"])
        blacklist = "|".join(value["blacklist"])

        whitelist_string = f'["{poi_tag}"~"{whitelist}"]' if whitelist else ''
        blacklist_string = f'["{poi_tag}"!~"{blacklist}"]' if blacklist else ''

        tags_query_string += (f'nw["{poi_tag}"]{whitelist_string}{blacklist_string}'
                              f'(area.france)(area.ville);\n')

    tags_query_string += ");"

    # Building complete overpass query
    query = f'''
    [out:json][timeout:300];
    area["admin_level"="2"]["name"="France"]->.france;
    area["admin_level"="8"]["name"="{city}"]->.ville;
    
    {tags_query_string}
    out center meta;
        '''

    return query


def get_overpass_response(city, dict_osm_pois):
    query_overpass = build_overpass_query_pois_with_siret(city, dict_osm_pois)

    # Execute overpass query
    api = overpass.API(timeout=100)
    response_api = api.Get(query_overpass, responseformat=f"json", build=False)

    return response_api['elements']


# Haversine distance function
def haversine_distances(coords1, coords2):
    return haversine(coords1, coords2, unit=Unit.METERS)


def poi_clustering(json_data, max_distance_meters, min_num_sample):

    data = json.loads(json_data)

    # Extract coordinates, considering both 'node' and 'way' types
    coordinates = []
    for item in data:
        if item['type'] == 'node':
            coordinates.append([item['lat'], item['lon']])
        elif item['type'] == 'way' and 'center' in item:
            coordinates.append([item['center']['lat'], item['center']['lon']])

    # Convert to numpy array
    coordinates = np.array(coordinates)

    # Compute the distance matrix
    distance_matrix = np.zeros((len(coordinates), len(coordinates)))
    for i in range(len(coordinates)):
        for j in range(len(coordinates)):
            if i != j:
                distance_matrix[i][j] = haversine_distances(coordinates[i], coordinates[j])

    # DBSCAN clustering
    # eps is the maximum distance between two samples for one to be considered as in the neighborhood of the other
    # min_samples is the number of samples in a neighborhood for a point to be considered as a core point
    dbscan = DBSCAN(eps=max_distance_meters, min_samples=min_num_sample, metric="precomputed")
    clusters = dbscan.fit_predict(distance_matrix)

    # Add cluster labels to the original data
    for i, item in enumerate(data):
        if 'type' in item and (item['type'] == 'node' or (item['type'] == 'way' and 'center' in item)):
            item['cluster'] = int(clusters[i])

    return data


def create_clustered_map(data, cluster_info, map_html_file):
    # Initialize a map with a center point
    if data[0]['type'] == 'node':
        center_lat = data[0]['lat']
        center_lon = data[0]['lon']
    else:  # Assuming it's a 'way' and has a 'center' key
        center_lat = data[0]['center']['lat']
        center_lon = data[0]['center']['lon']

    mymap = folium.Map(location=[center_lat, center_lon], tiles='CartoDB positron', zoom_start=14)

    # Define colors for the clusters
    colors = ['red', 'blue', 'green', 'purple', 'orange', 'darkred', 'lightred', 'gray',
              'darkblue', 'darkgreen', 'cadetblue', 'darkpurple', 'white', 'pink', 'lightblue',
              'lightgreen', 'beige', 'black', 'lightgray']

    # Create a dictionary to map cluster IDs to their info (distance and count)
    cluster_info_dict = {info[0]: (info[1], info[2]) for info in cluster_info}

    # Calculate the furthest distance from the centroid to any node in each cluster
    cluster_distances = {}

    for cluster_id, nodes in cluster_info_dict.items():
        centroid = calculate_centroid([node for node in data if node.get('cluster') == cluster_id])
        if centroid:
            furthest_distance = max(
                haversine(centroid, (node['lat'], node['lon'])) if node['type'] == 'node'
                else haversine(centroid, (node['center']['lat'], node['center']['lon']))
                for node in data if node.get('cluster') == cluster_id
            )
            cluster_distances[cluster_id] = furthest_distance

    # Add a semi-transparent circle for each cluster
    for cluster_id, (distance, count) in cluster_info_dict.items():
        centroid = calculate_centroid([node for node in data if node.get('cluster') == cluster_id])
        if centroid:
            cluster_color = colors[cluster_id % len(colors)]
            furthest_distance = cluster_distances[cluster_id]
            folium.Circle(
                location=centroid,
                radius=furthest_distance * 1030,  # Convert km to meters + extra
                color=cluster_color,
                stroke=False,
                fill=True,
                fill_color=cluster_color,
                fill_opacity=0.2
            ).add_to(mymap)

    # Iterate through the JSON data and add markers to the map
    for node in data:
        # Check the type and extract lat, lon accordingly
        if node['type'] == 'node':
            lat = node['lat']
            lon = node['lon']
        elif node['type'] == 'way' and 'center' in node:
            lat = node['center']['lat']
            lon = node['center']['lon']
        else:
            continue  # Skip this item if it doesn't have coordinate information

        cluster = node.get('cluster')
        if cluster is not None and cluster >= 0:  # Check if the cluster label exists and is not noise
            # Choose color for the marker based on the cluster label
            marker_color = colors[cluster % len(colors)]  # Use modulo to cycle through colors

            svg_icon = f"""
            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" viewBox="0 0 12 12">
                <circle cx="6" cy="6" r="5" fill="{marker_color}" />
            </svg>
            """

            # Create a Popup object hyperlink to OSM
            osm_url = f"https://www.openstreetmap.org/{node['type']}/{node['id']}"
            # Use the SVG icon as the marker icon
            icon_html = f'<a href="{osm_url}" target="_blank">{svg_icon}</a>'
            icon = folium.DivIcon(html=icon_html)
            # Add marker to the map with a popup
            folium.Marker(
                location=[lat, lon],
                icon=icon
            ).add_to(mymap)

    # Add a label for each cluster's centroid
    for cluster_id, (distance, count) in cluster_info_dict.items():
        # Find the centroid of the current cluster
        centroid = calculate_centroid([node for node in data if node.get('cluster') == cluster_id])
        if centroid:
            adjusted_centroid = (centroid[0] - 0.0001, centroid[1])  # Subtract a small value from the latitude
            commerce_text = "commerce" if count == 1 else "commerces"
            # Create the label text
            label_text = f"{count} {commerce_text}"
            # Create a folium DivIcon for the text label
            icon = folium.DivIcon(html=f'''
                <div centroid-lat="{centroid[0]}" centroid-lon="{centroid[1]}"
                style="background: rgba(255, 255, 255, 0.7); padding-left: 2px; padding-right: 115px;
                    padding-top: 2px; padding-bottom: 2px; border-radius: 6px; box-sizing: border-box;
                    font-size: 13pt; color: black; white-space: nowrap; text-align: center; border: 1px solid #000000;">
                    {label_text}
                </div>''')
            # Add a marker with the DivIcon to the map at the centroid
            folium.Marker(location=adjusted_centroid, icon=icon).add_to(mymap)

    folium.plugins.LocateControl().add_to(mymap)

    # Save the map to an HTML file
    mymap.save(map_html_file)


# Allows js to catch the user location, so we can calculate cluster distances
def create_global_var_map_folium(maphtml):
    # Now, read the generated HTML file, modify it, and write the changes back
    with open(maphtml, 'r') as file:
        filedata = file.read()

    # Find the line where the map object is initialized
    # This is a bit tricky because Folium generates a unique map object name every time
    # We look for the pattern that Folium uses, which is typically "var map_" followed by a unique ID
    map_object_pattern = re.compile(r'var (map_[a-f0-9]+) = L.map\(')  # regex pattern to find the map object
    matches = map_object_pattern.search(filedata)

    if matches:
        map_object_name = matches.groups()[0]

        # Construct the pattern to match the entire map initialization block
        map_init_block_pattern = re.compile(r'(var ' + re.escape(map_object_name) + r' = L.map\([\s\S]+?\);)')

        # Add the line to set window.globalMap after the map object initialization block
        replacement_string = r'\1\n    window.globalMap = ' + map_object_name + ';'
        filedata = map_init_block_pattern.sub(replacement_string, filedata)

        # Write the modified HTML back to the file
        with open(maphtml, 'w') as file:
            file.write(filedata)

    else:
        print("No unique Folium map object found")


# Function to calculate the centroid of a cluster
def calculate_centroid(nodes):
    latitudes = []
    longitudes = []
    for node in nodes:
        if node['type'] == 'node':
            latitudes.append(node['lat'])
            longitudes.append(node['lon'])
        elif node['type'] == 'way' and 'center' in node:
            latitudes.append(node['center']['lat'])
            longitudes.append(node['center']['lon'])
    if latitudes and longitudes:
        return sum(latitudes) / len(latitudes), sum(longitudes) / len(longitudes)
    else:
        return None


def calculate_distance_user_cluster(data):

    # Manual user location
    user_lat = 0
    user_lon = 0
    user_location = (user_lat, user_lon)

    # Organize nodes by cluster
    clusters = {}
    for node in data:
        cluster_id = node.get('cluster')
        if cluster_id is not None and cluster_id >= 0:
            if cluster_id in clusters:
                clusters[cluster_id].append(node)
            else:
                clusters[cluster_id] = [node]

    # Calculate distance to each cluster and count items
    cluster_info = []
    for cluster_id, nodes in clusters.items():
        centroid = calculate_centroid(nodes)
        if centroid:
            distance = haversine(user_location, centroid)
            cluster_info.append((cluster_id, distance, len(nodes)))
        else:
            print(f"Cluster {cluster_id} has no centroid, Number of items: {len(nodes)}")

    # Define weights for distance and number of items
    # For example, if distance is twice as important as the number of items:
    weight_distance = 1.1
    weight_number_of_items = 1

    # Calculate a combined score for each cluster
    for i, (cluster_id, distance, count) in enumerate(cluster_info):
        score = (distance * weight_distance) - (count * weight_number_of_items)
        cluster_info[i] = (cluster_id, distance, count, score)

    # Sort clusters by the combined score
    sorted_clusters = sorted(cluster_info, key=lambda x: x[3])

    return sorted_clusters


def template_ctc_ui(city):

    template_content = f"""
    <section class="section is-title-bar py-4">
      <div class="level">
        <!-- Logo and home link -->
        <div class="level-left">
          <div class="level-item">
            <a href="https://www.complete-tes-commerces.fr">
              <img src="../../media/homepage.svg" style="width: 25px; height: auto;" 
              class="homepage-icon" alt="Complete Tes Commerces Logo">
            </a>
          </div>
          <div class="level-item">
            <h1 class="title is-size-4 pr-6" style="color: black;">
              / {city}
            </h1>
          </div>
          <!-- Tab navigation -->
          <div class="level-item">
            <div class="tabs is-boxed">
              <ul>
                  <li><a href="index.html">Ajout SIRET/Adresse</a></li>
                  <li><a href="siret_clos.html">SIRETs Clôturés</a></li>
                  <li class="is-active"><a href="commerces_vides.html">Commerces Désaffectés</a></li>          
              </ul>
            </div>
          </div>
        </div>
      </div>
    </section>
    """
    return template_content


def create_disused_poi_frontend(city):

    city_lowered = city.lower().replace(" ", "-")
    html_content = create_bulma_theme(city)
    html_content += f"""<body>"""
    html_content += template_ctc_ui(city)

    html_content += f"""
<section class="section ">
    <div class=".container.is-widescreen">      
        <!-- Question mark icon with tooltip -->
        <div class="help-icon-container" style="display: flex; justify-content: space-between; 
                                                                align-items: center; margin: 20px 0;">
            <div style="flex-grow: 1; text-align: center;">
                <!-- The osmLink will be inserted here in the center -->
            </div>
            <div class="tooltip" style="flex-shrink: 0;">
                <img src="../../media/question-mark.svg" class="question-mark" alt="Help" style="vertical-align: middle; 
                                                                transform: scale(1.5); transform-origin: center;">
                <span class="tooltiptext has-text-left">
                    - Commerces OSM marqués comme 'disused' ou 'vacant' depuis > 6 mois  (ou avec un tag 
                    'check_date' > 6 mois).<br><br>
                    - Groupage par proximité pour cibler les zones principales.<br><br>
                    - Géolocalisation peut etre activée pour afficher la distance de ces groupes.
                </span>
            </div>
        </div>

    <!-- Map container -->
    <div class="box custom-map-box"> <!-- Adjust height as needed -->
        <!-- Embed the Folium map using an iframe -->
        <iframe class="map-iframe" src="disused_map.html" frameborder="0" style="border:0; 
                                                    width: 100%;" allowfullscreen></iframe>
    </div>
</div>
</section>
  <script>
  window.closedSirets = `../../ville/{city_lowered}/json/{city_lowered}_osm_closed_siret.json`;
  window.disusedPOIs = `../../ville/{city_lowered}/json/{city_lowered}_osm_disused_shops.json`;
  </script>
<script src="../../js/closed_pois.js"></script>
<script src="../../js/toggle_theme.js"></script>
</body>
</html>"""

    with open(f'website/ville/{city_lowered}/commerces_vides.html', "w") as disused_file:
        disused_file.write(html_content)


def parse_date(date_str):
    try:
        return datetime.datetime.strptime(date_str, "%Y-%m-%d")
    except ValueError:
        # Return None if the date format is incorrect
        return None


def run_disused_shops(city_name=None, city_code=None):
    now = datetime.datetime.utcnow()
    # today_month_year = datetime.datetime.today().strftime("%Y-%m")
    six_months = datetime.timedelta(days=6 * 30)  # Approximate 6 months

    if city_name:
        try:
            city_code = get_insee_code_from_city_name(city_name)
        except IndexError:
            sys.exit(f"Error: No Insee City Code found for {city_name}, please check city name again "
                     f"in data/v_commune_2023.csv.")
        except Exception as e:
            sys.exit(f"Error: {str(e)}")
    elif city_code:
        city_name = get_city_name_from_insee_code(city_code)

    city_lowered = city_name.lower().replace(" ", "-")
    json_file_disused_shops = f'website/ville/{city_lowered}/json/{city_lowered}_osm_disused_shops.json'
    map_file = f'website/ville/{city_lowered}/disused_map.html'

    dict_osm_pois_tags = {
        "disused:shop": {
            "whitelist": [],
            "blacklist": ["kiosk"]
        },
        "shop": {
            "whitelist": ["vacant"],
            "blacklist": []
        },
        "disused:amenity": {
            "whitelist": ["pub", "bar", "cafe", "restaurant", "fast_food", "nightclub", "bank", "ice_cream",
                          "car_rental", "stripclub", "theatre"],
            "blacklist": ["public_bookcase", 'public_building']
        },
        "amenity": {
            "whitelist": ["vacant"],
            "blacklist": ["public_bookcase", 'public_building']
        }
    }

    print(f"Getting disused and vacants shops in OSM for {city_name}")
    print(f"  |---> Retrieving disused shops/amenities")
    # Retrieve the overpass response using your functions
    overpass_response = get_overpass_response(city_name, dict_osm_pois_tags)

    print("  |---> Filtering out disused for > 6 months")
    # Filter out the data
    filtered_data = [
        item for item in overpass_response
        if 'timestamp' in item and
           (now - datetime.datetime.strptime(item['timestamp'], "%Y-%m-%dT%H:%M:%SZ") > six_months) and
           ('tags' not in item or 'check_date' not in item['tags'] or
            (parse_date(item['tags']['check_date']) is None or
             now - parse_date(item['tags']['check_date']) > six_months))
    ]

    # Count the original and filtered items
    original_count = len(overpass_response)
    filtered_count = len(filtered_data)
    filtered_out_count = original_count - filtered_count

    if len(filtered_data) == 0:
        print("        No shop left to process")
        print("  |---> Finished")
        nothing_html = create_bulma_theme(city_name)
        nothing_html += f"""<body>"""
        nothing_html += template_ctc_ui(city_name)
        nothing_html += """
        <br>
        <br>
        <br>
        <center>Aucun commerce désaffecté</center>
        </body>
        </html>
        """
        with open(f'website/ville/{city_lowered}/commerces_vides.html', "w") as disused_file:
            disused_file.write(nothing_html)
    else:
        print("  |---> Looking for clusters")

        filtered_json = json.dumps(filtered_data, indent=4)
        clustered_json = poi_clustering(filtered_json, 350, 1)

        # Save the new data with cluster labels to a file
        with open(json_file_disused_shops, 'w') as outfile:
            json.dump(clustered_json, outfile, indent=2)

        # Collect all cluster values
        cluster_values = [node.get('cluster') for node in clustered_json if 'cluster' in node]
        unique_clusters = set(cluster_values)

        print("  |---> Creating map webpage")
        distance_cluster_json = calculate_distance_user_cluster(clustered_json)
        create_clustered_map(clustered_json, distance_cluster_json, map_file)
        create_global_var_map_folium(map_file)

        create_disused_poi_frontend(city_name)

        # Print info
        print("  |---> Finished")
        print(f"  Items kept: {filtered_count} - Filtered out: {filtered_out_count} - Clusters: {len(unique_clusters)}")


def validate_city_insee_code(code):
    if len(code) != 5 or not (code[0].isdigit() and code[2:].isdigit() and (code[1].isdigit() or code[1].isalpha())):
        raise argparse.ArgumentTypeError("City code must be 5 digits")
    return code


if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser(description='Gather OSM disused shops')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-n', '--name', type=str, help='the name of the city')
    group.add_argument('-c', '--code', type=validate_city_insee_code, help='the city code (5-digit)')
    args = parser.parse_args()

    if args.name:
        run_disused_shops(city_name=args.name)
    elif args.code:
        run_disused_shops(city_code=args.code)
