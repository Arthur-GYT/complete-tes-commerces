from logger import logger
from unidecode import unidecode
import pandas as pd
import numpy as np
import csv


# Get a dataframe of potential matches if there is a specific shop name on a specific street
def candidates_match_dataframe(shop_name, shop_street, tuple_housenum_minmax, csvfile):

    matching_items = []
    name_provided = False if shop_name == '' else True
    street_provided = False if shop_street == '' else True

    if tuple_housenum_minmax is not None:
        min_housenum = tuple_housenum_minmax[0]
        max_housenum = tuple_housenum_minmax[1]
    else:
        min_housenum = ''
        max_housenum = ''

    try:
        with open(csvfile, 'r') as file:
            reader = csv.DictReader(file)

            for row in reader:

                tag_list = {
                    'enseigne1Etablissement': row['enseigne1Etablissement'] + row['denominationUsuelleEtablissement'],
                    'numeroVoieEtablissement': row['numeroVoieEtablissement'],
                    'indiceRepetitionEtablissement': row['indiceRepetitionEtablissement'],
                    'typeVoieEtablissement': row['typeVoieEtablissement'],
                    'libelleVoieEtablissement': row['libelleVoieEtablissement'],
                    'dateCreationEtablissement': row['dateCreationEtablissement'],
                    'siret': row['siret'],
                    'dateDernierTraitementEtablissement': row['dateDernierTraitementEtablissement'].split()[0] if row[
                        'dateDernierTraitementEtablissement'] else '',
                    'etatAdministratifEtablissement': row['etatAdministratifEtablissement'],
                    'complementAdresseEtablissement': row['complementAdresseEtablissement'],
                    'activitePrincipaleEtablissement': row['activitePrincipaleEtablissement']
                }

                if name_provided and street_provided:
                    # BUG: OR operator returns **first** True value found only. so won't get to the denomination thing
                    # if shop_name.lower() in (
                    #         row['enseigne1Etablissement'].lower() or row['denominationUsuelleEtablissement'].lower())
                    #         and shop_street.lower() in row['libelleVoieEtablissement'].lower():
                    if (shop_name.lower() in row['enseigne1Etablissement'].lower() or
                        shop_name.lower() in row['denominationUsuelleEtablissement'].lower()) and \
                            shop_street.lower() in row['libelleVoieEtablissement'].lower():
                        # Check if row['numeroVoieEtablissement'] is between min_housenum and max_housenum
                        if min_housenum and max_housenum:
                            try:
                                house_num = int(row['numeroVoieEtablissement'])
                                if min_housenum <= house_num <= max_housenum:
                                    matching_items.append(tag_list)
                            except (ValueError, TypeError):  # when 'numeroVoieEtablissement' is not number or is None
                                pass
                        else:
                            matching_items.append(tag_list)

                if name_provided and not street_provided:
                    if (shop_name.lower() in row['enseigne1Etablissement'].lower() or
                            shop_name.lower() in row['denominationUsuelleEtablissement'].lower()):
                        matching_items.append(tag_list)
                        # Check if row['numeroVoieEtablissement'] is between min_housenum and max_housenum
                        if min_housenum and max_housenum:
                            try:
                                house_num = int(row['numeroVoieEtablissement'])
                                if min_housenum <= house_num <= max_housenum:
                                    matching_items.append(tag_list)
                            except (ValueError, TypeError):  # when 'numeroVoieEtablissement' is not a number or is None
                                pass
                        else:
                            matching_items.append(tag_list)

                if not name_provided and street_provided:
                    if shop_street.lower() in row['libelleVoieEtablissement'].lower():
                        matching_items.append(tag_list)
                        # Check if row['numeroVoieEtablissement'] is between min_housenum and max_housenum
                        if min_housenum and max_housenum:
                            try:
                                house_num = int(row['numeroVoieEtablissement'])
                                if min_housenum <= house_num <= max_housenum:
                                    matching_items.append(tag_list)
                            except (ValueError, TypeError):  # when 'numeroVoieEtablissement' is not a number or is None
                                pass
                        else:
                            matching_items.append(tag_list)

        if len(matching_items) == 0:
            log_matches.append(f"[ ]{shop_name.upper()}")

        else:
            log_matches.append(f"[X]{shop_name.upper()}")
            return pd.DataFrame(matching_items)

    except FileNotFoundError:
        return print("INSEE CSV File Was Not Found.")


# Get a dataframe of potential matches for any part of a shop name together with a specific street
def splitword_match_dataframe(city_name, shop_name, street_name_housenum, csvfile):

    df_list = []
    ignored_prepositions = ['les', 'des', 'sur', 'par', 'aux']
    street_name = street_name_housenum[0]
    minmax_hounum = street_name_housenum[1]

    shop_name = shop_name.replace(".", " ").replace("-", " ").replace("'", " ").replace(":", " ").replace("*", " ")
    street_name = street_name.replace(".", " ").replace("-", " ").replace("'", " ").replace(":", " ").replace("*", " ")
    street_last_word = street_name.split()[-1]

    # Get dataframe with shop matches (currently checking every shop words against *last word* of street name)
    for word in shop_name.split():
        if word.lower() != city_name.lower():  # 5-10% faster if ignore city name in shop names (also less rubbish out)
            if len(word) > 2 and word.lower() not in ignored_prepositions:
                result = candidates_match_dataframe(unidecode(word), unidecode(street_last_word), minmax_hounum, csvfile)

                df_list.append(result)

    dfconc = pd.concat(df_list)  # Concatenate all DataFrames

    logger.info(f"     {' '.join(log_matches)} : {street_name}")

    # If no matches were found, return an empty DataFrame
    if dfconc is None:
        return pd.DataFrame()

    return dfconc


# Gather all dataframes of shop names together for each street candidate
def get_dataframe_matches(city, shop_name, dict_of_streets, csvfile):

    df_list2 = []

    for i in range(len(dict_of_streets)):
        street_name = dict_of_streets[i][0].replace("-", " ")
        dict_of_streets[i] = [street_name, dict_of_streets[i][1]]

    # Loop to gather all dataframes of shop names together for each street candidate
    for street_name_housenum in dict_of_streets:

        # Testing overall logging
        global log_matches
        log_matches = []

        # Find shop name parts in the Sirene CSV file and return dataframe if some matches were found
        try:
            df_temp = splitword_match_dataframe(city, shop_name, street_name_housenum, csvfile)
            df_list2.append(df_temp)
        except Exception as e:
            # print(f"Error when processing street name {street_name}: {e}")
            pass

    # Concatenate all dataframes from the loop together
    try:
        return pd.concat(df_list2)
    except Exception as e:
        # print(f"Error when concatenating dataframes: {e}")
        pass


def dataframe_clean_filter_prettify(shop_name, df_to_clean):

    # Sort by most exact match, then most recent check and finally creation date // Also drop items with no name
    df_to_clean['is_match'] = df_to_clean['enseigne1Etablissement'].str.lower() == shop_name.lower()
    df_to_clean.sort_values(by=['is_match', 'dateDernierTraitementEtablissement', 'dateCreationEtablissement'],
            ascending=[False, False, False], inplace=True)

    df_to_clean['enseigne1Etablissement'] = df_to_clean['enseigne1Etablissement'].replace('', np.nan)
    df_to_clean = df_to_clean.dropna(subset=['enseigne1Etablissement'])
    df_to_clean = df_to_clean.drop('is_match', axis=1)
    df_to_clean['enseigne1Etablissement'] = df_to_clean['enseigne1Etablissement'].replace('nan ', '', regex=True)

    # Probably can remove the complementAdresseEtablissement from below since we have above line
    df_to_clean = df_to_clean.dropna(subset=['libelleVoieEtablissement', 'complementAdresseEtablissement'])

    # Removing all shops that closed down - 'A' for open, 'F' for closed
    df_sirene_open = df_to_clean[df_to_clean['etatAdministratifEtablissement'] != 'F']

    df_sirene_open = df_sirene_open.copy()

    # Clarify/Prettify Table and drop old columns
    df_sirene_open['name'] = df_sirene_open['enseigne1Etablissement']
    df_sirene_open['adress'] = (df_sirene_open['numeroVoieEtablissement'] +
                                df_sirene_open['indiceRepetitionEtablissement'] + ', ' +
                                df_sirene_open['typeVoieEtablissement'] + ' ' +
                                df_sirene_open['libelleVoieEtablissement'])
    df_sirene_open['numero siret'] = df_sirene_open['siret']
    df_sirene_open['last checked'] = df_sirene_open['dateDernierTraitementEtablissement']
    df_sirene_open['created'] = df_sirene_open['dateCreationEtablissement']
    df_sirene_open['Statut'] = df_sirene_open['etatAdministratifEtablissement']  # Added to show Biz Started/Closed Down
    df_sirene_open['InseeCategory'] = df_sirene_open['activitePrincipaleEtablissement']  # Business INSEE category

    # Dropping the original columns and removing duplicates found due to shop name splitting
    df_sirene_open.drop(df_sirene_open.columns[0:10], axis=1, inplace=True)
    df_sirene_open = df_sirene_open.drop_duplicates()

    return df_sirene_open


def get_all_good_sirene_matches(city, pois_df, csv_sirene_data):

    total_pois = len(pois_df)
    pois_df = pois_df.copy()
    pois_df['sirene'] = None

    for poi_index, poi_row in pois_df.iterrows():

        # Show % progress
        progress_percentage = (poi_index / total_pois) * 100
        print(f'\r |---> Progress: {progress_percentage:.1f}%', end='')

        osm_id = poi_row['osm_id']
        shop_name = unidecode(poi_row['name'])
        osm_type = poi_row['osm_type']
        potential_streets_housenum_dict = poi_row['housenum']

        if potential_streets_housenum_dict is not None:
            potential_streets = [street[0] for street in poi_row['housenum'] if street and street[0]]
        else:
            potential_streets = []

        logger.info(f"{shop_name.upper()} - https://www.osm.org/{osm_type}/{osm_id}")

        if not potential_streets:
            logger.info(f"     No street found around POI")

        else:
            try:
                no_street_counter = 0
                df = get_dataframe_matches(city, shop_name, potential_streets_housenum_dict, csv_sirene_data)

                if df is None:
                    no_street_counter = no_street_counter + 1
                    logger.info(f"     No matches in Sirene DB")
                    pass

                elif df is not None and not df.empty:

                    final_df = dataframe_clean_filter_prettify(shop_name, df)

                    # Print the dataframe if we still have items left after sorting/droping
                    if len(final_df) == 0:
                        logger.info(f'     Matches filtered out (most likely closed down already)')
                    else:
                        final_df = final_df.reset_index(drop=True)
                        dict_matches = final_df.to_dict('index')
                        pois_df.loc[poi_index, 'sirene'] = [dict_matches]

                if no_street_counter == len(potential_streets):
                    logger.info(f'     No matches in Sirene DB')

            except ValueError as e:
                logger.error(f' /!\\  VALUE ERROR: {e}')

    return pois_df
