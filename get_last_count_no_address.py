from datetime import datetime, timedelta, date
from pre_processing import get_insee_code_from_city_name, get_city_name_from_insee_code
import sys
import os
import json
import argparse


def validate_city_insee_code(code):
    if len(code) != 5 or not (code[0].isdigit() and code[2:].isdigit() and (code[1].isdigit() or code[1].isalpha())):
        raise argparse.ArgumentTypeError("City code must be 5 digits")
    return code


# Parse arguments
parser = argparse.ArgumentParser(description='Get last count POIs without address')
group = parser.add_mutually_exclusive_group(required=True)  # Ensure either -n or -c is provided
group.add_argument('-n', '--name', type=str, help='the name of the city')
group.add_argument('-c', '--code', type=validate_city_insee_code, help='the city code (5-digit)')
args = parser.parse_args()


city_queried = None
city_insee_code = None

if args.name:
    city_queried = args.name
elif args.code:
    city_insee_code = args.code

if city_queried:
    try:
        city_insee_code = get_insee_code_from_city_name(city_queried)
    except IndexError:
        sys.exit(f"Error: No Insee City Code found for {city_queried}, please check city name again "
                 f"in data/v_commune_2023.csv.")
    except Exception as e:
        sys.exit(f"Error: {str(e)}")
elif city_insee_code:
    city_queried = get_city_name_from_insee_code(city_insee_code)


def get_last_count_no_address():
    last_date_no_address = None
    last_date_no_siret = None
    last_date_no_name = None
    city_lowered = city_queried.lower().replace(" ", "-")
    city_json_array = f'website/ville/{city_lowered}/json/{city_lowered}_dailystats.json'

    # Check if the JSON file exists
    if os.path.exists(city_json_array):
        with open(city_json_array, 'r') as f:
            data = json.load(f)

        # Get last count of shops without address
        last_date_no_address = data[-1]['no_address']
        last_date_no_siret = data[-1]['no_siret']
        last_date_no_name = data[-1]['no_name']

    print(f"{city_lowered},{last_date_no_address},{city_queried},{last_date_no_siret},{last_date_no_name}")


if __name__ == "__main__":
    get_last_count_no_address()
