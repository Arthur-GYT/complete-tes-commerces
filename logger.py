import logging

# Create a logger object.
logger = logging.getLogger('my_logger')

# Set the level of this logger.
# The level is a threshold. It only logs events that are this level or higher.
# WARNING is a level. The levels are DEBUG, INFO, WARNING, ERROR, CRITICAL in increasing order.
logger.setLevel(logging.INFO)

# Create a file handler object.
file_handler = logging.FileHandler('data/debug.log', 'a')

# Create a formatter object.
formatter = logging.Formatter('%(message)s')

# Set the formatter for the file handler.
file_handler.setFormatter(formatter)

# Add the file handler to the logger.
logger.addHandler(file_handler)
