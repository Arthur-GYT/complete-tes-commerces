# Complète Tes Commerces

Concentrez-vous sur l'exhaustivité des données - Laissez l'ajout de nouveaux POIs aux autres.

Un projet qui vise à compléter et mettre à jour les tags de commerces déjà présents dans OpenStreetMap.
L'utilisateur voit un nombre de choix restreint pour chaque POI, ce qui facilite et accélère la prise de décision sur 
ce qu'il convient de contribuer à OSM (ou non). Cet outil apporte des informations ciblées, mais sans garantie. 

## Pourquoi ?

Il existe déjà de très bons outils qui permettent d'ajouter des commerces manquants sur OSM tel que 
[Banco](http://commerces.openstreetmap.fr) et [Osmose](https://osmose.openstreetmap.fr/fr/map/), mais ils ne permettent pas de compléter des commerces existants et ils ne sont pas si 
facile à utiliser pour les débutants.

Au fur-et-à-mesure qu'OSM croît, il est de plus en plus probable que les commerces soient déjà présents, 
l'exhaustivité et l'actualisation des tags prend alors une plus grande importance. 


## Fonctionnement Général

Ce script est conçu pour être utilisé à l'échelle d'une ville, permettant ainsi une concentration sur l'environnement proche
et facilitant la vérification des informations sur le terrain.

Il y a trois fonctions principales :
* **Intégration des tags adresse/SIRET** - rapprochement des commerces, disposant d'un tag `name` mais sans adresse, avec la base Sirene.
* **Actualisation des SIRET non actifs** - propositions de nouveaux SIRETs pour les commerces qui sont signalés comme fermés dans la base Sirene.
* **Identification des commerces désaffectés** - mise en évidence des commerces marqués comme `disused` ou `vacant` depuis > 6 mois.

Lors du premier lancement, le script télécharge automatiquement le dernier fichier Sirene.
Si la base de données a plus d'un mois, le script télécharge une mise à jour afin d'obtenir le detail des nouveaux commerces.

## Installation et Configuration

Le jeu de données présent dans le fichier zip de [data.gouv.fr](https://www.data.gouv.fr/en/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/) n'est pas complet. 

Il faut donc créer un compte sur l'[API Store](https://api.insee.fr/catalogue/site/themes/wso2/subthemes/insee/pages/item-info.jag?name=Sirene&version=V3&provider=insee) de l'Insee afin de récupérer clé API et secret et les ajouter aux variables d'environnement `SIRENE_API_KEY` et `SIRENE_API_SECRET` (voir ci-dessous).

Ce projet peut fonctionner sans cette API, mais elle est vivement recommandée pour un meilleur rapprochement des données.

```
git clone https://gitlab.com/meryl_street/complete-tes-commerces.git
cd complete-tes-commerces/
python -m venv env
source env/bin/activate
pip install -r requirements.txt
export SIRENE_API_KEY=VOTRE_CLE_API
export SIRENE_API_SECRET=VOTRE_SECRET_API
chmod +x ctc-osm.py
```

## Utilisation

`./ctc-osm.py main --name <nom_ville>` pour cibler les commerces avec des adresses et SIRETs manquants dans OSM.

`./ctc-osm.py find_old_shops --name <nom_ville>` pour cibler les commerces désaffectés et/ou avec un SIRET clos.

`./ctc-osm.py city_attic_stats --name <nom_ville>` pour actualiser les statistiques des tags de commerces.

## Options Avancées

`--code <code_insee_ville>` au lieu de `--name <nom_ville>`

`main --prevent-update-insee` pour éviter de télécharger la nouvelle base Sirene & de re-synchroniser l'API Insee.

## Licence

[MIT LICENSE](LICENSE)
