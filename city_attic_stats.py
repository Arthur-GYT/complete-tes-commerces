from datetime import datetime, timedelta, date
import os
import sys
import time
import json
import overpass
import argparse
from pre_processing import get_insee_code_from_city_name, get_city_name_from_insee_code


def get_overpass_response(city_queried, dateandtime):

    query_overpass = f'''
    
    [date:"{dateandtime}"][out:csv(::type,::count)][timeout:200];
    area["admin_level"="2"]["name"="France"]->.france;
    area["admin_level"="8"]["name"="{city_queried}"]->.ville;
    
    // shops and amenities in city
    (
    nw["shop"]["shop"!~"kiosk|vacant"](area.france)(area.ville);
    nw["amenity"]["amenity"~"pub|bar|cafe|restaurant|fast_food|nightclub|bank|ice_cream|car_rental|stripclub|theatre"]["amenity"!~"public_bookcase|public_building"](area.france)(area.ville);
    )->.total;
    
    // filter those without names, hours, websites
    (nw.total[!"name"]["name:signed"!~"no"](area.france)(area.ville);)->.without_name;
    (nw.total[!"opening_hours"](area.france)(area.ville);)->.without_hours;
    (nw.total[!"contact:website"][!"website"](area.france)(area.ville);)->.without_website;
    
    // From these city's shops/amenities, get only ones without an address defined and a name signage
    ( 
      nw.total["contact:street"!~"."]["addr:street"!~"."]["addr:place"!~"."]["name:signed"!~"no"](area.france)(area.ville);
      nw.total["contact:housenumber"!~"."]["addr:housenumber"!~"."]["addr:place"!~"."]["name:signed"!~"no"](area.france)(area.ville);
    )->.amenityshop_without_address;
       
    // Select all provides_feature relations, where one of the shop/amenity is a member
    // Then for thoses relations, find all related node members
    // Then same for way members (building areas)
    rel["type"="provides_feature"](bn.amenityshop_without_address);
    node(r)->.node_amenityshop_in_relation_prov;
    rel["type"="provides_feature"](bw.amenityshop_without_address);
    way(r)->.way_amenityshop_in_relation_prov;
    
    rel["type"="associatedStreet"](bn.amenityshop_without_address);
    node(r)->.node_amenityshop_in_relation_assoc;
    rel["type"="associatedStreet"](bw.amenityshop_without_address);
    way(r)->.way_amenityshop_in_relation_assoc;

    // Combine both node/way and get the ones not part of relations
    ( .node_amenityshop_in_relation_prov; .way_amenityshop_in_relation_prov; .node_amenityshop_in_relation_assoc; 
                                            .way_amenityshop_in_relation_assoc;)->.amenityshop_in_relation;
    (( .amenityshop_without_address; - .amenityshop_in_relation;);)->.without_address;
    
    (nw.total[!"ref:FR:SIRET"](area.france)(area.ville);)->.without_siret;
    
    // Don't forget these are later defined in that order in python dictionnary
    .total out count;
    .without_name out count;
    .without_hours out count;
    .without_website out count;
    .without_address out count;
    .without_siret out count;
    '''

    api = overpass.API(timeout=200)
    response = api.Get(query_overpass, verbosity='geom', build=False)

    return response


def validate_city_insee_code(code):
    if len(code) != 5 or not (code[0].isdigit() and code[2:].isdigit() and (code[1].isdigit() or code[1].isalpha())):
        raise argparse.ArgumentTypeError("City code must be 5 digits")
    return code


def run_city_attic_stats(city_name=None, city_code=None):
    city_queried = city_name
    city_insee_code = city_code

    if city_queried:
        try:
            city_insee_code = get_insee_code_from_city_name(city_queried)
        except IndexError:
            sys.exit(f"Error: No Insee City Code found for {city_queried}, please check city name again "
                     f"in data/v_commune_2023.csv.")
        except Exception as e:
            sys.exit(f"Error: {str(e)}")
    elif city_insee_code:
        city_queried = get_city_name_from_insee_code(city_insee_code)

    city_lowered = city_queried.lower().replace(" ", "-")
    city_json_array = f'website/ville/{city_lowered}/json/{city_lowered}_dailystats.json'

    # Check if the JSON file exists
    if os.path.exists(city_json_array):
        with open(city_json_array, 'r') as f:
            data = json.load(f)

        # Find the last date in the data and define the start date for overpass query
        last_date_str = data[-1]['date']
        last_date = datetime.strptime(last_date_str, "%Y-%m-%dT%H:%M:%SZ").date()
        start_date = last_date + timedelta(days=1)
    else:
        # File doesn't exist, we need to build historical data (2 months prior)
        few_months_ago = datetime.today().replace(day=1) - timedelta(days=60)
        start_date = date(few_months_ago.year, few_months_ago.month, 1)
        data = []

    end_date = datetime.today().date()
    timeout_duration = 5

    # Loop from the start date to the end date
    current_date = start_date

    print(f"Downloading historical stats for {city_queried}")

    while current_date <= end_date:

        formatted_date = current_date.strftime("%Y-%m-%dT%H:%M:%SZ")
        try:
            # Get the overpass response for the current date
            result = get_overpass_response(city_queried, formatted_date)
            if result is None:
                # API call failed error handling
                print(f"API call failed for date {formatted_date}. Exiting and saving data collected so far.")
                break

            # Check if the result has enough data to extract
            if len(result) >= 6:
                # Don't forget below are defined in that order in overpass query without any name to differenciate them
                dict_stats = {
                    'city': city_queried,
                    'date': formatted_date,
                    'total': int(result[1][1]),
                    'no_name': int(result[2][1]),
                    'no_hours': int(result[3][1]),
                    'no_website': int(result[4][1]),
                    'no_address': int(result[5][1]),
                    'no_siret': int(result[6][1]),
                }
                data.append(dict_stats)
                print(dict_stats)
            else:
                print(f"There was an issue with extract stats on date {formatted_date}.")
                dict_stats = {'status': 'error', 'date': formatted_date}
        except Exception as e:
            # Handle any other exceptions that might occur
            print(f"An error occurred: {e}. Exiting and saving data collected so far.")
            break
        else:
            # Increment the date by one day
            current_date += timedelta(days=1)
        finally:
            time.sleep(timeout_duration)

    # Ensure that the directory exists before writing the file
    os.makedirs(os.path.dirname(city_json_array), exist_ok=True)

    # Save the updated data to the JSON file
    with open(city_json_array, 'w') as f:
        json.dump(data, f)

    exit()


# This allows the script to be run both as a standalone script and as a module
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Gather historical tags stats')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-n', '--name', type=str, help='the name of the city')
    group.add_argument('-c', '--code', type=validate_city_insee_code, help='the city code (5-digit)')
    args = parser.parse_args()

    if args.name:
        run_city_attic_stats(city_name=args.name)
    elif args.code:
        run_city_attic_stats(city_code=args.code)
