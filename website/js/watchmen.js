//////////////////////////////////////////////////////////////////
// Create tiles with Sirene matched data & handle JOSM links
//////////////////////////////////////////////////////////////////
fetch(window.jsonFilePath)
  .then(response => response.json())
  .then(data => {
    const container = document.createElement('div');
    container.classList.add('columns', 'is-multiline'); // 'is-multiline' allows wrapping of columns
    document.querySelector('.tile-container').appendChild(container);

    const getFirstUrl = (shop) => {
      const contactWebsites = shop.contactwebsite ? shop.contactwebsite.split(',') : [];
      const websites = shop.website ? shop.website.split(',') : [];
      const firstContactWebsite = contactWebsites.find(url => url.trim() !== '');
      const firstWebsite = websites.find(url => url.trim() !== '');

      return firstContactWebsite || firstWebsite;
    };

    for (const key in data) {
      const shop = data[key];

      const tile = document.createElement('div');
      tile.classList.add('column', 'is-two-fifth', 'card', 'has-background-white-ter', 'is-flex', 'is-flex-direction-column', 'mx-1');
      tile.style.borderColor = 'rgba(60, 60, 60, 0.2)';
      const cardHeader = document.createElement('header');
      cardHeader.classList.add('card-header');
      const cardHeaderTitle = document.createElement('h2');
      cardHeaderTitle.classList.add('card-header-title', 'has-less-padding', 'no-side-padding');
      cardHeaderTitle.style.fontSize = "1.5em";
      cardHeaderTitle.textContent = shop.name;

      // Create hyperlink osm poi website
      const firstUrl = getFirstUrl(shop);
      if (firstUrl) {
        const hyperlink = document.createElement('a');
        hyperlink.href = firstUrl;
        hyperlink.target = '_blank';
        hyperlink.rel = 'noopener noreferrer';
        hyperlink.innerHTML = '<img src="../../media/website.svg" class="website-icon" alt="URL" />';
        hyperlink.style.marginLeft = '7px';
        hyperlink.style.marginTop = '3px';
        hyperlink.classList.add('icon-link');
        cardHeaderTitle.appendChild(hyperlink);
      }

      cardHeader.appendChild(cardHeaderTitle);
      tile.appendChild(cardHeader);

      // Create icon element
      const infoIcon = document.createElement('span');
      infoIcon.classList.add('icon', 'is-clickable');
      infoIcon.innerHTML = '<img src="../../media/arrow-down.svg" class="arrow-icon" alt="OSM Tags" />';
      infoIcon.style.marginLeft = '7px'; // Move it to the left by 10 pixels
      cardHeaderTitle.appendChild(infoIcon);

      // Create mini-table for OSM tags
      const createMiniTable = (shop) => {
        const table = document.createElement('table');
        table.classList.add('table-container');
        table.classList.add('table', 'is-narrow');
        const tbody = document.createElement('tbody');
        tbody.classList.add('table');
        table.appendChild(tbody);

        // Add a row for the 'osm tags' title
        const titleRow = document.createElement('tr');
        const titleCell = document.createElement('th');
        titleCell.colSpan = 2;
        titleCell.innerHTML = '<strong>Tags OSM</strong>';
        titleRow.appendChild(titleCell);
        tbody.appendChild(titleRow);

        const possibleKeys = ['amenity', 'shop', 'housetag', 'addrtag', 'siret'];

          // Iterate over the possible keys and add a row for each present key
          possibleKeys.forEach((key) => {
            if (shop.hasOwnProperty(key) && shop[key].trim() !== '') {
              const tr = document.createElement('tr');
              const keyTd = document.createElement('td');
              keyTd.textContent = `${key} =`; // Use the key name in the cell
              const valueTd = document.createElement('td');
              valueTd.textContent = shop[key]; // Use the value associated with the key
              tr.appendChild(keyTd);
              tr.appendChild(valueTd);
              tbody.appendChild(tr);
            }
          });

          return table;
        };

        // Check for the siret property after iterating through all possible keys
        if (shop.hasOwnProperty('siret') && shop.siret.trim() !== '') {
          tile.classList.add('siret-tile');
        }

        // Add event listener to the icon
        infoIcon.addEventListener('click', (event) => {
          event.stopPropagation(); // Prevent click from immediately propagating to document
          let miniTableContainer = cardHeader.querySelector('.table-container');
          if (!miniTableContainer) {
            // Create and insert mini-table container if it doesn't exist
            miniTableContainer = createMiniTable(shop);
            cardHeader.appendChild(miniTableContainer); // Append to cardHeader instead of cardContent
          }
          // Toggle visibility
          miniTableContainer.style.display = miniTableContainer.style.display === 'block' ? 'none' : 'block';
        });

        // Add event listener to the document to hide the table when clicking elsewhere
        document.addEventListener('click', (event) => {
          let miniTableContainer = cardHeader.querySelector('.table-container');
          if (miniTableContainer && miniTableContainer.style.display === 'block') {
            // Check if the click is outside the miniTableContainer
            if (!miniTableContainer.contains(event.target) && event.target !== infoIcon) {
              miniTableContainer.style.display = 'none';
            }
          }
        });

      tile.appendChild(cardHeader);

      const cardContent = document.createElement('div');
      cardContent.classList.add('card-content', 'is-flex', 'is-justify-content-space-between', 'has-negative-margin');
      cardContent.style.paddingTop = "0.1rem";
      cardContent.style.paddingBottom = "0.3rem";

      if (shop.sirene) {

        // Create two columns
        const textColumn = document.createElement('div');
        textColumn.style.marginRight = "-32px"; // Have Sirene db items take as much space as possible
        textColumn.classList.add('column');

        const mapColumn = document.createElement('div');
        mapColumn.classList.add('column');
        mapColumn.style.paddingLeft = '0px';
        mapColumn.style.marginLeft = '21px';
        const textContainer = document.createElement('div');
        textContainer.classList.add('text-container');

        for (const sireneKey in shop.sirene) {
          const sirene = shop.sirene[sireneKey];
          for (const sireneItemKey in sirene) {
            const sireneItem = sirene[sireneItemKey];

            const radioDiv = document.createElement('div');
            radioDiv.style.display = 'block';

            const radio = document.createElement('input');
            radio.style.marginLeft = "-10px"; // Added this line
            radio.style.marginRight = "3px"; // Added this line
            radio.type = 'radio';
            radio.name = 'shop';

            const label = document.createElement('label');
            label.title = `Commerce potentiel provenant de la base Sirene`;

            // Append the radio button as a child of the label
            radioDiv.appendChild(radio);

            // Construct the path to the Insee category .svg
            const formattedInseeCategory = sireneItem["InseeCategory"].replace('.', '');
            const iconPath = `../../media/poicons/${formattedInseeCategory}.svg`;

            // Create an img element for the SVG
            const svgImage = document.createElement('img');
            svgImage.src = iconPath;
            svgImage.alt = sireneItem["InseeCategory"];
            svgImage.classList.add('icon-class');
            svgImage.style.marginRight = "3px"; // Added this line

            // Event listener to handle the error if the SVG does not exist
            svgImage.onerror = function() {
            };

            // Event listener to handle successful loading of the SVG
            svgImage.onload = function() {
                // Append the SVG image to the label only if it loads successfully
                radioDiv.insertBefore(svgImage, radio.nextSibling);
            };

            // Create a span for the label text and append it to the label
            const labelText = document.createElement('span');
            labelText.classList.add('label-text');
            const siretNumber = sireneItem["numero siret"];
            const siretLink = `https://annuaire-entreprises.data.gouv.fr/etablissement/${siretNumber}`;
            labelText.innerHTML = `<strong>${sireneItem.name}</strong><br>${sireneItem.adress}<br>SIRET:
                                            <a href="${siretLink}" target="_blank">${siretNumber}</a>`;
            label.appendChild(labelText);
            radioDiv.appendChild(label);

            // Adding the icon to the label
            const icon = document.createElement('img');
            icon.src = '../../media/copy-paste.svg';
            icon.title = `Cliquez-moi pour copier le Siret dans le presse-papiers`;
            icon.classList.add('copy-icon');
            label.appendChild(icon);

            icon.addEventListener('click', () => {
              navigator.clipboard.writeText(sireneItem["numero siret"])
                .then(() => {
                  console.log('Numéro de Siret copié dans le presse-papiers');
                })
                .catch(err => {
                  console.error('Impossible de copier le numéro de Siret: ', err);
                });
            });

            // Add event listener to radio button
            radio.addEventListener('change', function() {
              if (this.checked) {
                const testsiret = sireneItem["numero siret"];
                josmButton.href = `http://localhost:8111/load_and_zoom?left=${minlon}&right=${maxlon}&top=${maxlat}&bottom=${minlat}&select=${shop['osm_type']}${shop['osm_id']}&addtags=ref:FR:SIRET=${testsiret}`;
                josmButton.title = `C'est parti`;
                console.log('Radio button selected siret ' + testsiret);
              }
            });

            radioDiv.appendChild(label);

            textContainer.appendChild(radioDiv);
          }
        }

        const minimap = document.createElement('div');
        minimap.classList.add('minimap', 'is-flex', 'is-justify-content-flex-end');
        const imgContainer = document.createElement('div');
        imgContainer.className = 'image-container';

        const img = document.createElement('img');
        img.style.marginLeft = '7px'; // Move it to the left by 10 pixels
        img.src = `img/${shop['osm_type'].slice(0, 1)}${shop['osm_id']}.jpg`;
        img.alt = `Map image for ${shop.name}`;
        img.title = `Cliquez-moi pour aller vers OSM`;

        const imglink = document.createElement('a');
        imglink.href = 'https://www.openstreetmap.org/' + shop['osm_type'] + '/' + shop['osm_id'];
        imglink.target = "_blank";
        imglink.appendChild(img);

        imgContainer.appendChild(imglink);
        minimap.appendChild(imgContainer);

        textColumn.appendChild(textContainer);
        mapColumn.appendChild(minimap);

        cardContent.appendChild(textColumn);
        cardContent.appendChild(mapColumn);

      }
      tile.appendChild(cardContent);

        const cardFooter = document.createElement('footer');
        cardFooter.classList.add('card-footer');
        cardFooter.style.marginTop = 'auto';

        // Create the first button for the ID link
        const idButton = document.createElement('a');
        idButton.classList.add('id-button', 'card-footer-item', 'button'); // Style as a button
        idButton.href = 'https://www.openstreetmap.org/edit?editor=id&lat=' + shop['osm_lat'] + '&lon=' +
                        shop['osm_lon'] + '&zoom=19&' + shop['osm_type'] + '=' + shop['osm_id'];
        idButton.target = "_blank"; // opens the link in a new tab
        idButton.textContent = 'Éditer avec OSM iD';
        // Add a right margin to the button (adjust the size as needed, e.g., 'mr-2' for a larger space)
        idButton.classList.add('mr-4');

        idButton.addEventListener('click', function(event){
              this.style.background = "#981b29";
              this.style.color = "black";
              this.style.boxShadow = "none";
        });

        // Append the ID button to the card footer
        cardFooter.appendChild(idButton);

        let prevTile = null; // global variable to store the previous tile

        // Create the second button for the JOSM link
        const josmButton = document.createElement('a');
        josmButton.classList.add('josm', 'card-footer-item', 'button');
        josmButton.textContent = 'Éditer avec JOSM';
        josmButton.title = `Choisissez d'abord une des options`;

        const minlon = parseFloat(shop['osm_lon']) - 0.0005;
        const maxlon = parseFloat(shop['osm_lon']) + 0.0005;
        const minlat = parseFloat(shop['osm_lat']) - 0.0005;
        const maxlat = parseFloat(shop['osm_lat']) + 0.0005;

        josmButton.addEventListener('click', function(event){
            // Return early if href is not defined
            if (!josmButton.href || josmButton.href === "") {
              return;
            }
              this.style.background = "#981b29";
              this.style.color = "black";
              this.style.boxShadow = "none";

            event.preventDefault(); // prevents opening the link in a new page

            // deactivate the href of the previous tile
            if (prevTile) {
              prevTile.removeAttribute('href');
            }

            // add 'clicked' class only if href is defined
            if(josmButton.href) {
              const img = document.createElement('img');
              img.src = josmButton.href;
              buttons.classList.add('clicked-josm');
            }

            // store the current tile as the previous tile for the next click event
            prevTile = josmButton;
        });

        // Append the JOSM button to the card footer
        cardFooter.appendChild(josmButton);

        // Append the cardFooter to the tile
        tile.appendChild(cardFooter);

        container.appendChild(tile);
    }
  })
  .catch(error => console.error('Error fetching shop data:', error));


//////////////////////////////////////////////////
//    Toggle tiles with/without siret number    //
//////////////////////////////////////////////////
document.addEventListener('DOMContentLoaded', function() {
  // Get the checkbox element
  var checkbox = document.getElementById('toggleSiretTiles');

  // Set the initial state of the checkbox to unchecked
  checkbox.checked = false;

  // Event listener for when the checkbox state changes
  checkbox.addEventListener('change', function() {
    // Get all elements with the 'siret-tile' class
    const siretTiles = document.querySelectorAll('.siret-tile');

    // If the checkbox is checked, hide the tiles, otherwise show them
    if (this.checked) {
      // Checkbox is checked, hide the tiles
      siretTiles.forEach(tile => {
        tile.style.setProperty('display', 'none', 'important');
      });
    } else {
      // Checkbox is unchecked, show the tiles
      siretTiles.forEach(tile => {
        tile.style.removeProperty('display');
      });
    }
  });
});
//////////////////////////////////////////////////
// Show some quick stats about the last script run
//////////////////////////////////////////////////
fetch(window.lastStatsPath)
  .then(response => response.json())
  .then(data => {
    const monthNames = ["Janvier", "Fevrier", "Mars", "Avril", "Mai", "Juin",
      "Juillet", "Aout", "Septembre", "Octobre", "Novembre", "Décembre"];

    const date = new Date(data["date"]);
    const formattedDate = date.getDate() + ' ' + monthNames[date.getMonth()];
    const inseeDate = (date.getYear() + 1900) + '-' + (date.getMonth() + 1).toString().padStart(2, '0');

    const scriptLastRunBox = document.getElementById('script-last-run-datetime');
    scriptLastRunBox.innerHTML = `
      <div class="has-text-centered">
        <span class="has-text-weight-bold is-size-4">${formattedDate}</span>
        <span><br>----------------</span>
        <div>Dernière mise à jour - source INSEE ${inseeDate}</div>
      </div>`;

    const matchingRateBox = document.getElementById('matching-rate-sentence');
    matchingRateBox.innerHTML = `
      <div class="has-text-centered">
        <span class="has-text-weight-bold is-size-4">${data["%_estimated_matching_rate"]}%</span>
        <span><br>----------------</span>
        <div>Taux de correspondance estimé avec la base Sirene</div>
      </div>`;

    const poisWithoutNameBox = document.getElementById('pois-without-name-sentence');
    poisWithoutNameBox.innerHTML = `
      <div class="has-text-centered">
        <span class="has-text-weight-bold is-size-4">${data.pois_without_name}</span>
        <span><br>----------------</span>
        <div>Commerces sans nom à intégrer pour traitement automatique</div>
      </div>`;
    const overpassUrlPoisWithoutName = poisWithoutNameBox.dataset.url;

    const aElement = document.createElement('a');
    aElement.href = overpassUrlPoisWithoutName;
    aElement.target = "_blank";
    aElement.innerHTML = `
        <div class="has-text-centered">
        Où sont-ils ?
         </div>`;
    poisWithoutNameBox.appendChild(aElement);

    // Construct the progress bar using Bulma 'progress' components
    const statsBarContainer = document.getElementById('stats-bar-container');
    const progressBarHeight = '6rem';
    const borderRadius = '6px';

    const totalSegments = data.pois_with_one_sirene_match + data.pois_with_many_sirene_match + data.pois_with_no_sirene_match + data.pois_without_name;
    const oneSireneMatchPercent = (data.pois_with_one_sirene_match + data.pois_with_many_sirene_match) / totalSegments * 100;
    const withoutNamePercent = data.pois_without_name / totalSegments * 100;
    const noSireneMatchPercent = data.pois_with_no_sirene_match / totalSegments * 100;

statsBarContainer.innerHTML = `
  <div class="progress-bars">
    <div class="progress-bar" style="width: ${oneSireneMatchPercent}%; background-color: #1f5d64;">
      <p class="progress-text" style="color: #e3f0f1;">${oneSireneMatchPercent > 20 ? `${data.pois_with_one_sirene_match + data.pois_with_many_sirene_match} commerces potentiellement trouvés` : `${data.pois_with_one_sirene_match + data.pois_with_many_sirene_match} trouvés`}</p>
    </div>
    ${withoutNamePercent > 0 ? `
    <div class="progress-bar" style="width: ${withoutNamePercent}%; background-color: #7fa5a9;">
      <p class="progress-text" style="color: #e3f0f1;">${withoutNamePercent > 20 ? `${data.pois_without_name} commerces sans nom` : `${data.pois_without_name} sans nom`}</p>
    </div>` : ''}
    <div class="progress-bar" style="width: ${noSireneMatchPercent}%; background-color: #a4c3c8;">
      <p class="progress-text" style="color: #e3f0f1;">${noSireneMatchPercent > 20 ? `${data.pois_with_no_sirene_match} commerces ne peuvent être retrouvés dans la base Sirene` : `${data.pois_with_no_sirene_match} non trouvés`}</p>
    </div>
  </div>`;
    })
  .catch(error => console.error('Error fetching last stats data:', error));

///////////////////////////////////////////////
// Code for sorting dashboard table columns
///////////////////////////////////////////////
document.addEventListener('DOMContentLoaded', () => {
    const getCellValue = (tr, idx) => tr.children[idx].innerText || tr.children[idx].textContent;
    const comparer = (idx, asc) => (a, b) => ((v1, v2) =>
        v1 !== '' && v2 !== '' && !isNaN(v1) && !isNaN(v2) ? v1 - v2 : v1.toString().localeCompare(v2)
    )(getCellValue(asc ? a : b, idx), getCellValue(asc ? b : a, idx));

    // Do the work when the document is loaded
    document.querySelectorAll('th[data-sort]').forEach(th => th.addEventListener('click', (() => {
        const table = th.closest('table');
        const tbody = table.querySelector('tbody');
        Array.from(tbody.querySelectorAll('tr'))
            .sort(comparer(Array.from(th.parentNode.children).indexOf(th), this.asc = !this.asc))
            .forEach(tr => tbody.appendChild(tr) );
    })));
});

///////////////////////////////////////////////
// Plot long term evolution of tags within city
///////////////////////////////////////////////
let myChart; // Define myChart in a higher scope
fetch(window.dailyStatsPath)
    .then(response => response.json())
    .then(data => {
      const canvas = document.getElementById('big-line-chart');
      const ctx = canvas.getContext('2d');

      // Only display date labels every X days
      const dates = data.map((item, index) => index % 5 === 0 ? new Date(item.date).toLocaleDateString('fr-FR', { month: 'short', day: 'numeric' }) : '');
      const with_address = data.map(item => ((item.total - item.no_address) / item.total * 100).toFixed(1));
      const with_name = data.map(item => ((item.total - item.no_name) / item.total * 100).toFixed(1));
      const with_siret = data.map(item => ((item.total - item.no_siret) / item.total * 100).toFixed(1));

      let color = getComputedStyle(document.body).getPropertyValue('--color-text').trim();

      myChart = new Chart(ctx, {
        type: 'line',
        data: {
          labels: dates,
          datasets: [
            {
              label: '% avec nom',
              data: with_name,
              fill: false,
              borderColor: 'rgb(255, 99, 132)',
              tension: 0.1,
              pointRadius: 1,
              pointBorderColor: 'rgb(255, 99, 132)',
              pointBorderWidth: 4,
            },
            {
              label: '% avec adresse',
              data: with_address,
              fill: false,
              borderColor: 'rgb(75, 192, 192)',
              tension: 0.1,
              pointRadius: 1,
              pointBorderColor: 'rgb(75, 192, 192)',
              pointBorderWidth: 4,
            },
            {
              label: '% avec SIRET',
              data: with_siret,
              fill: false,
              borderColor: 'rgb(179, 189, 189)',
              tension: 0.1,
              pointRadius: 1,
              pointBorderColor: 'rgb(179, 189, 189)',
              pointBorderWidth: 4,
            }
          ]
        },
        options: {
          responsive: true, // Enable automatic resizing
          maintainAspectRatio: false,
          scales: {
            x: {
              type: 'category',
              ticks: {
                color: '#4e4949',
                maxRotation: 0,
                minRotation: 0
                },
              grid: {
                  drawOnChartArea: false,
                },
              display: true,
                },
            y: {
              ticks: {
                color: '#4e4949',
                callback: function(value) {
                  return value + "%";
                },
                },
              grid: {
                  drawOnChartArea: false,
                },
              display: true,
                },
             },
          plugins: {
              legend: {
                labels: {
                  color: '#4e4949'
                }
              }
            },
        }
      });
    })
    .catch(error => console.error('Error fetching data:', error));

///////////////////////////////////////////////
// Burger Menu with Bulma
///////////////////////////////////////////////
document.addEventListener('DOMContentLoaded', () => {
  // Get all "navbar-burger" elements
  const navbarBurgers = Array.prototype.slice.call(document.querySelectorAll('.navbar-burger'), 0);

  // Check if there are any navbar burgers
  if (navbarBurgers.length > 0) {
    // Add a click event on each of them
    navbarBurgers.forEach(el => {
      el.addEventListener('click', () => {
        // Get the target from the "data-target" attribute
        const target = el.dataset.target;
        const $target = document.getElementById(target);

        // Toggle the "is-active" class on both the "navbar-burger" and the "navbar-menu"
        el.classList.toggle('is-active');
        $target.classList.toggle('is-active');
      });
    });
  }
});