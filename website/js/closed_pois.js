// Function to create a tile for each parent SIRET
function createTile(parentSiret, parentData) {
    const tileParent = document.createElement('div');
    tileParent.className = 'tile is-parent is-3';

    const tileChild = document.createElement('article');
    tileChild.className = 'tile is-child box has-background-white-ter';
    tileChild.style.display = 'flex';
    tileChild.style.flexDirection = 'column';
    tileChild.style.justifyContent = 'space-between';
    tileChild.style.height = '100%';
    tileChild.style.borderStyle = 'solid';
    tileChild.style.borderWidth = '0.1px';
    tileChild.style.borderColor = 'rgba(60, 60, 60, 0.2)';

    // Create a container for the title and the icon
    const titleContainer = document.createElement('div');
    titleContainer.className = 'title has-text-centered is-align-items-center mb-0';

    // Create the title text
    const titleText = document.createTextNode(parentData.osm_name);

    // Create the hyperlink with an icon
    const titleLink = document.createElement('a');
    titleLink.href = `https://www.openstreetmap.org/${parentData.osm_type}/${parentData.osm_id}`;
    titleLink.target = '_blank';

    // Create the icon and add it to the hyperlink
    const icon = document.createElement('img');
    icon.src = '../../media/ext-link-black.svg';
    icon.style.width = '20px';
    icon.style.marginLeft = '7px';
    icon.className = 'ext-link';

    // Append the title text and the hyperlink icon to the title container
    titleContainer.appendChild(titleText);
    titleLink.appendChild(icon);
    titleContainer.appendChild(titleLink);

    // Check if poi_website is not null and create a hyperlink with an icon
    if (parentData.poi_website) {
        // Create the hyperlink for the poi_website
        const websiteLink = document.createElement('a');
        websiteLink.href = parentData.poi_website;
        websiteLink.target = '_blank';
        websiteLink.style.marginLeft = '10px'; // Add some space between the links

        // Create the icon for the website link
        const websiteIcon = document.createElement('img');
        websiteIcon.src = '../../media/website.svg';
        websiteIcon.style.width = '20px';
        websiteIcon.className = 'website-link website-icon';

        // Append the icon to the hyperlink
        websiteLink.appendChild(websiteIcon);

        // Append the website hyperlink to the title container after titleLink
        titleContainer.appendChild(websiteLink);
    }

    // Append the title container to the tile
    tileChild.appendChild(titleContainer);

    // Create and append the Bulma separator after the title
    const titleSeparator = document.createElement('hr');
    titleSeparator.className = 'hr';
    tileChild.appendChild(titleSeparator);


    const content = document.createElement('div');
    content.className = 'content';
    content.style.flexGrow = '1';

    // Create a separate list for the parent SIRET
    const parentUl = document.createElement('div');
    parentUl.className = 'px-1';
    const parentLi = document.createElement('div');
    const parentSvgImage = createSvgImage(parentData.code_naf);
    parentUl.style.backgroundColor = '#fff3e6';
    parentUl.style.borderStyle = 'solid';
    parentUl.style.borderWidth = '0.1px';
    parentUl.style.borderRadius = '3px';

    // Set up the error handling for the SVG image
    parentSvgImage.onerror = function() {
        // Create a text node for the NAF code and insert it after parentLink
        const codeNafText = document.createTextNode(` ${parentData.code_naf}`);
        parentLink.after(codeNafText);
        // Remove the SVG image since it doesn't exist
        parentSvgImage.remove();
    };

    const parentLink = document.createElement('a');
    parentLink.href = `https://annuaire-entreprises.data.gouv.fr/etablissement/${parentSiret}`;
    parentLink.textContent = parentSiret;
    parentLink.target = '_blank';

    // Append the SVG image to the list item
    parentLi.appendChild(parentSvgImage);

    // Append the parentLink to the list item
    parentLi.appendChild(parentLink);

    // Create the "closed" icon
    const closedIcon = document.createElement('img');
    closedIcon.src = '../../media/locked.svg';
    closedIcon.alt = 'Fermé';
    closedIcon.style.width = '20px';

    // Construct the parent list item content
    parentLi.appendChild(document.createTextNode(`${parentData.denomination} `));
    parentLi.appendChild(document.createElement('br'));
    parentLi.appendChild(parentLink);
    parentLi.appendChild(document.createElement('br'));

    // Append the "closed" icon and the closing date to the list item
    parentLi.appendChild(closedIcon);

    const createParentDate = parentData.last_change_date
    const dateParentParts = createParentDate.split('-');
    const formattedDate = `${dateParentParts[2]}/${dateParentParts[1]}/${dateParentParts[0]}`;
    parentLi.appendChild(document.createTextNode(` Fermé le ${formattedDate}`));

    // Append the list item to the unordered list
    parentUl.appendChild(parentLi);

    const childUl = document.createElement('div');
    parentData.open_sirets.forEach(childObj => {
        const childSiret = Object.keys(childObj)[0];
        const childData = childObj[childSiret];
        const childLi = document.createElement('div');
        childLi.className = 'my-1 px-1 rounded-5px';

        // Create SVG image or text node depending on the existence of the SVG
        const childSvgImage = createSvgImage(childData.code_naf);
        childSvgImage.onerror = function() {
            // Create a text node for the NAF code and insert it after childLink
            const codeNafText = document.createTextNode(` ${childData.code_naf}`);
            childLink.after(codeNafText);
            // Remove the SVG image since it doesn't exist
            childSvgImage.remove();
        };
        // Append the SVG image to the list item
        childLi.appendChild(childSvgImage);
        childLi.style.backgroundColor = '#e8f5e9';
        childLi.style.borderStyle = 'solid';
        childLi.style.borderWidth = '0.1px';
        childLi.style.borderRadius = '3px';

        const childLink = document.createElement('a');
        childLink.href = `https://annuaire-entreprises.data.gouv.fr/etablissement/${childSiret}`;
        childLink.textContent = childSiret;
        childLink.target = '_blank';

        // Check if the first part of the child's code_naf matches the parent's code_naf
        if (childData.code_naf.substring(0, 5) === parentData.code_naf.substring(0, 5)) {
            childLi.style.fontWeight = 'bold';
        }

        // Create the "unlocked" icon
        const openIcon = document.createElement('img');
        openIcon.src = '../../media/unlocked.svg';
        openIcon.style.width = '20px';

        // Construct the child list item content
        childLi.appendChild(document.createTextNode(`${childData.denomination} `));
        childLi.appendChild(document.createElement('br'));
        childLi.appendChild(childLink);
        childLi.appendChild(document.createElement('br'));

        // Append the "unlocked" icon and the opening date to the list item
        childLi.appendChild(openIcon);
        const createDate = childData.create_date;
        const dateParts = createDate.split('-');
        const formattedDate = `${dateParts[2]}/${dateParts[1]}/${dateParts[0]}`;
        childLi.appendChild(document.createTextNode(` Ouvert le ${formattedDate}`));

        // Append the list item to the unordered list
        childUl.appendChild(childLi);
    });

    content.appendChild(parentUl);

    // Create an image element for the arrow down
    const arrowContainer = document.createElement('div');
    arrowContainer.className = 'has-text-centered';
    const arrowDownImage = document.createElement('img');
    arrowDownImage.src = '../../media/arrow-down.svg';
    arrowDownImage.alt = 'Arrow down';
    arrowDownImage.className = 'arrow-down';

    // Append the arrow down image to the content
    arrowContainer.appendChild(arrowDownImage);
    content.appendChild(arrowContainer);

    content.appendChild(childUl);

    // Assuming you want to append the buttons after the childUl
    const tileFooter = document.createElement('footer');
    tileFooter.className = 'card-footer';

    // Create first button
    const buttonOne = document.createElement('a');
    buttonOne.className = 'button id-button is-dark card-footer-item mx-1 is-left';
    buttonOne.textContent = 'OSM iD';
    buttonOne.href = `https://www.openstreetmap.org/edit?editor=id&lat=${parentData['osm_lat']}&lon=${parentData['osm_lon']}&zoom=19&${parentData['osm_type']}=${parentData['osm_id']}`;
    buttonOne.target = '_blank';
    buttonOne.rel = 'noopener noreferrer';

    buttonOne.addEventListener('click', function(event){
                  this.style.background = "#981b29";
                  this.style.color = "black";
                  this.style.boxShadow = "none";
            });

    // Calculate the bounding box coordinates based on parentData's longitude and latitude
    const minlon = parseFloat(parentData['osm_lon']) - 0.0005;
    const maxlon = parseFloat(parentData['osm_lon']) + 0.0005;
    const minlat = parseFloat(parentData['osm_lat']) - 0.0005;
    const maxlat = parseFloat(parentData['osm_lat']) + 0.0005;

    // Create second button
    const buttonTwo = document.createElement('a');
    buttonTwo.className = 'button josm is-dark card-footer-item mx-1 is-right';
    buttonTwo.textContent = 'JOSM';
    buttonTwo.href = `http://localhost:8111/load_and_zoom?left=${minlon}&right=${maxlon}&top=${maxlat}` +
                     `&bottom=${minlat}&select=${parentData['osm_type']}${parentData['osm_id']}`;
    buttonTwo.target = '_blank';

    buttonTwo.addEventListener('click', function(event){
                // Return early if href is not defined
                if (!buttonTwo.href || buttonTwo.href === "") {
                  return;
                }
                  this.style.background = "#981b29";
                  this.style.color = "black";
                  this.style.boxShadow = "none";

                event.preventDefault(); // prevents opening the link in a new page

                // add 'clicked' class only if href is defined
                if(buttonTwo.href) {
                  const img = document.createElement('img');
                  img.src = buttonTwo.href;
                }
    });

    // Append buttons to the footer
    tileFooter.appendChild(buttonOne);
    tileFooter.appendChild(buttonTwo);

    tileChild.appendChild(content);
    tileChild.appendChild(tileFooter);
    tileParent.appendChild(tileChild);

    return tileParent;
}

// Function to load JSON data and create tiles
function loadAndDisplayTiles() {
    fetch(window.closedSirets)
        .then(response => {
            if (!response.ok) {
                throw new Error(`HTTP error! status: ${response.status}`);
            }
            return response.json();
        })
        .then(data => {
            // Containers for categorized tiles
            const easyContainer = document.getElementById('easy-container');
            const unsureContainer = document.getElementById('unsure-container');
            const unknownContainer = document.getElementById('unknown-container');

            // Check if containers exist
            if (!easyContainer || !unsureContainer || !unknownContainer) {
                throw new Error('One or more tile containers are missing in the DOM.');
            }

            // Function to create a new tile row
            function createTileRow() {
                const tileRow = document.createElement('div');
                tileRow.className = 'tile is-ancestor';
                return tileRow;
            }

            // Initialize tile rows for each category
            let easyTileRow = createTileRow();
            let unsureTileRow = createTileRow();
            let unknownTileRow = createTileRow();

            let easyTileCount = 0;
            let unsureTileCount = 0;
            let unknownTileCount = 0;

            let nodeIds = [];
            let wayIds = [];

            // Clear existing tiles in the containers
            easyContainer.innerHTML = '';
            unsureContainer.innerHTML = '';
            unknownContainer.innerHTML = '';

            // Process the data to create tiles, sort them into categories
            for (const parentSiret in data) {
                const parentData = data[parentSiret];

                // Collect osm_id based on osm_type
                if (parentData.osm_type === 'node') {
                    nodeIds.push(parentData.osm_id);
                } else if (parentData.osm_type === 'way') {
                    wayIds.push(parentData.osm_id);
                }

                const tile = createTile(parentSiret, parentData);

                // Append the tile to the appropriate container and row based on the match field
                switch (parentData.match) {
                    case 'no_survey_needed':
                        if (easyTileCount % 4 === 0 && easyTileCount !== 0) {
                            easyContainer.appendChild(easyTileRow);
                            easyTileRow = createTileRow();
                        }
                        easyTileRow.appendChild(tile);
                        easyTileCount++;
                        break;
                    case 'same_shop_type_possible_name_change':
                        if (unsureTileCount % 4 === 0 && unsureTileCount !== 0) {
                            unsureContainer.appendChild(unsureTileRow);
                            unsureTileRow = createTileRow();
                        }
                        unsureTileRow.appendChild(tile);
                        unsureTileCount++;
                        break;
                    case 'more_complex_case':
                        if (unknownTileCount % 4 === 0 && unknownTileCount !== 0) {
                            unknownContainer.appendChild(unknownTileRow);
                            unknownTileRow = createTileRow();
                        }
                        unknownTileRow.appendChild(tile);
                        unknownTileCount++;
                        break;
                    default:
                        console.warn(`Unknown match type '${parentData.match}' for SIRET ${parentSiret}`);
                        break;
                }
            }

            // Append any remaining tiles in the last row
            if (easyTileRow.children.length > 0) {
                easyContainer.appendChild(easyTileRow);
            }
            if (unsureTileRow.children.length > 0) {
                unsureContainer.appendChild(unsureTileRow);
            }
            if (unknownTileRow.children.length > 0) {
                unknownContainer.appendChild(unknownTileRow);
            }

            // After processing all tiles, check if the easy-container is empty
            if (easyContainer.children.length === 0) {
                // If it is empty, hide the entire block containing the easy-container
                const easyBlock = easyContainer.parentNode;
                easyBlock.style.display = 'none';
            }

            // Repeat the same check for the other containers if needed
            if (unsureContainer.children.length === 0) {
                const unsureBlock = unsureContainer.parentNode;
                unsureBlock.style.display = 'none';
            }

            if (unknownContainer.children.length === 0) {
                const unknownBlock = unknownContainer.parentNode;
                unknownBlock.style.display = 'none';
            }

            // Append the last rows if they have tiles
            if (easyTileCount % 4 !== 0) {
                easyContainer.appendChild(easyTileRow);
            }
            if (unsureTileCount % 4 !== 0) {
                unsureContainer.appendChild(unsureTileRow);
            }
            if (unknownTileCount % 4 !== 0) {
                unknownContainer.appendChild(unknownTileRow);
            }

            // Construct the Overpass API hyperlink
            const baseOverpassUrl = 'https://overpass-turbo.eu/?Q=%5Bout%3Ajson%5D%5Btimeout%3A100%5D%3B%0A%28%0A++';
            const overpassQueryNodes = nodeIds.length > 0 ? `node(id:${nodeIds.join(',')});%0A` : '';
            const overpassQueryWays = wayIds.length > 0 ? `way(id:${wayIds.join(',')});%0A` : '';
            const overpassQuery = `${overpassQueryNodes}${overpassQueryWays}%29%3B%0Aout+geom%3B`;
            const overpassUrl = `${baseOverpassUrl}${overpassQuery}&R`;

            const osmLink = document.createElement('a');
            osmLink.href = overpassUrl;
            osmLink.textContent = 'Où sont ces commerces ?';
            osmLink.target = '_blank';
            osmLink.className = 'button is-normal';

            // Select the help-icon-container
            const helpIconContainer = document.querySelector('.help-icon-container');

            // Select the center div where the osmLink will be placed
            const centerDiv = helpIconContainer.children[0];

            // Append the OSM link to the center div
            centerDiv.appendChild(osmLink);

            // Append any remaining tiles in the last row
            if (easyTileRow.children.length > 0) {
                easyContainer.appendChild(easyTileRow);
            }

        })

        .catch(error => {
            console.error('Error fetching or parsing the data:', error);
        });
}

// Function to create an SVG image element for a given code_naf
function createSvgImage(code_naf) {
    // Remove the period and construct the file path
    const formattedCodeNAF = code_naf.replace('.', '');
    const iconPath = `../../media/poicons/${formattedCodeNAF}.svg`;

    // Create an img element for the SVG
    const svgImage = document.createElement('img');
    svgImage.src = iconPath;
    svgImage.alt = code_naf;
    svgImage.classList.add('icon-class');
    svgImage.style.marginRight = "3px";

    // Event listener to handle the error if the SVG does not exist
    svgImage.onerror = function() {
        // Replace the image with a text node containing the NAF code
        const textNode = document.createTextNode(code_naf);
        svgImage.replaceWith(textNode);
    };

    return svgImage;
}

// Ensure this function is called after the DOM has fully loaded
document.addEventListener('DOMContentLoaded', function() {
    loadAndDisplayTiles();
});

fetch(window.closedSirets)
    .then(response => response.json())
    .then(data => {
        // Count the number of items
        const itemCount = Object.keys(data).length;

        // Find the "SIRET Expiré" tab
        const siretTab = document.querySelector('a[href="siret_clos.html"]');

        // Create a new span element with the count
        const countElement = document.createElement('span');
        countElement.textContent = itemCount;
        countElement.className = 'tag is-small';
        countElement.style.marginLeft = '5px';

        // Add the count to the "SIRET Expiré" tab
        siretTab.appendChild(countElement);
    });

fetch(window.disusedPOIs)
    .then(response => response.json())
    .then(data => {
        // Extract node IDs and way IDs
        const nodeIds = data.filter(item => item.type === "node").map(item => item.id);
        const wayIds = data.filter(item => item.type === "way").map(item => item.id);

        // Count the number of items
        const itemCount = data.length;

        // Find the "Disused Shop" tab
        const disusedShopTab = document.querySelector('a[href="commerces_vides.html"]');

        // Create a new span element with the count
        const countElement = document.createElement('span');
        countElement.textContent = itemCount;
        countElement.className = 'tag is-small';
        countElement.style.marginLeft = '5px';

        // Add the count to the "Disused Shop" tab
        disusedShopTab.appendChild(countElement);

        // Only append the Overpass API hyperlink if on the 'commerces_vides.html' page
        if (window.location.href.includes('commerces_vides.html')) {
            // Construct the Overpass API hyperlink
            const baseOverpassUrl = 'https://overpass-turbo.eu/?Q=%5Bout%3Ajson%5D%5Btimeout%3A100%5D%3B%0A%28%0A++';
            const overpassQueryNodes = nodeIds.length > 0 ? `node(id:${nodeIds.join(',')});%0A` : '';
            const overpassQueryWays = wayIds.length > 0 ? `way(id:${wayIds.join(',')});%0A` : '';
            const overpassQuery = `${overpassQueryNodes}${overpassQueryWays}%29%3B%0Aout+geom%3B`;
            const overpassUrl = `${baseOverpassUrl}${overpassQuery}&R`;

            const osmLink = document.createElement('a');
            osmLink.href = overpassUrl;
            osmLink.textContent = 'Lien vers Overpass Turbo';
            osmLink.target = '_blank';
            osmLink.className = 'button is-normal';

            // Select the help-icon-container
            const helpIconContainer = document.querySelector('.help-icon-container');

            // Select the center div where the osmLink will be placed
            const centerDiv = helpIconContainer.children[0];

            // Append the OSM link to the center div
            centerDiv.appendChild(osmLink);
        }
    })
    .catch(error => {
        console.error('Error fetching disused POIs:', error);
    });


///////////// Disused parts
document.addEventListener('DOMContentLoaded', function() {
    var iframe = document.querySelector('iframe[src="disused_map.html"]');

    // Wait for the iframe to load
    iframe.onload = function() {
        var map = iframe.contentWindow.globalMap;

        // Function to update centroids with distance information
        function updateCentroids(latlng) {
            // Select all divs with centroid information
            var centroids = iframe.contentDocument.querySelectorAll('div[centroid-lat]');
            centroids.forEach(function(centroidDiv) {
                // Check if the distance has already been calculated
                if (!centroidDiv.hasAttribute('data-distance-updated')) {
                    var centroidLat = parseFloat(centroidDiv.getAttribute('centroid-lat'));
                    var centroidLon = parseFloat(centroidDiv.getAttribute('centroid-lon'));
                    // Calculate the distance between the user's location and the centroid
                    var distance = latlng.distanceTo([centroidLat, centroidLon]);
                    // Convert distance to kilometers and round to two decimal places
                    var distanceInKm = (distance / 1000).toFixed(1);
                    distanceInKm = distanceInKm.replace('.', ',');
                    centroidDiv.innerHTML += ' à ' + distanceInKm + ' km';
                    // Mark the div as updated
                    centroidDiv.setAttribute('data-distance-updated', 'true');
                    // Update padding-right style
                    centroidDiv.style.paddingRight = '187px';
                }
            });
        }

        // Now you can use the map object
        map.once('locationfound', function(e) {
            var userLat = e.latlng.lat;
            var userLng = e.latlng.lng;
            console.log("User's Latitude: " + userLat + ", Longitude: " + userLng);

            // Update centroids with distance information
            updateCentroids(e.latlng);
        });

        //map.on('locationerror', function(e) {
        //    alert(e.message);
        //});

        // Locate the user's position only once, without watching for changes
        //map.locate({maxZoom: 16, watch: false});

        var locateButton = iframe.contentDocument.querySelector('.leaflet-control-locate a');
        if (locateButton) {
            locateButton.addEventListener('click', function() {
                // Call locate only once when the button is clicked
                map.locate({maxZoom: 16, watch: false});
            });
        }
    };
});

