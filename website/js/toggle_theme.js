//////////////////////////////////////
////////// Dark Mode Toggle //////////
//////////////////////////////////////
function toggleTheme() {

  let themeButton = document.getElementById('toggle-border-color');
  let themeIcon = themeButton.querySelector('img'); // Get the img tag within the button

  document.body.classList.toggle('white-borders');

  let body = document.body;
  let navbarElements = document.querySelectorAll('.navbar');
  let columnCards = document.querySelectorAll('.column.card');
  let titles = document.querySelectorAll('.title, .card-header-title');
  let warnings = document.querySelectorAll('.warning-box');
  let labelTexts = document.querySelectorAll('.label-text');
  let table = document.querySelectorAll('.table');
  let labelTextsStrong = document.querySelectorAll('.label-text strong');
  let whiteButtons = document.querySelectorAll('.white-button');
  let tileChildren = document.querySelectorAll('.tile.is-child.box');

  let tabs = document.querySelector('.tabs');
  let tabItems = document.querySelectorAll('.tabs ul li');
  let tabLinks = document.querySelectorAll('.tabs ul li a');

  // Select the buttons by their classes
  let josmButtons = document.querySelectorAll('.josm');
  let idButtons = document.querySelectorAll('.id-button');

  if (body.getAttribute("data-theme") === "dark") {
    body.setAttribute("data-theme", "light");
    navbarElements.forEach(navbarElement => {
      navbarElement.style.backgroundColor = '#5c8084';
    });
    document.querySelectorAll('.copy-icon').forEach(function(icon) {
      icon.src = '../../media/copy-paste.svg';
    });
    document.querySelectorAll('.theme-icon').forEach(function(icon) {
      icon.src = '../../media/chart.svg';
    });
    document.querySelectorAll('.website-icon').forEach(function(icon) {
      icon.src = '../../media/website.svg';
    });
    document.querySelectorAll('.arrow-icon').forEach(function(icon) {
      icon.src = '../../media/arrow-down.svg';
    });
    document.querySelectorAll('.toggle-icon').forEach(function(icon) {
      icon.src = '../../media/theme-light-dark.svg';
    });
    columnCards.forEach(card => {
      card.classList.remove('has-background-grey-darker');
      card.classList.add('has-background-white-ter');
    });
    whiteButtons.forEach(button => { // New loop
      button.style.color = 'white';
    });
    document.querySelectorAll('.homepage-icon').forEach(function(icon) {
      icon.src = '../../media/homepage.svg';
    });
    titles.forEach(title => {
      title.style.color = 'black';
    });
    table.forEach(table => {
      table.style.backgroundColor = 'white';
    });
    warnings.forEach(warning => {
      warning.style.backgroundColor = 'black';
      warning.style.color = 'white';
    });
    labelTexts.forEach(labelText => {
      labelText.style.color = 'black';
    });
    labelTextsStrong.forEach(labelText => {
      labelText.style.color = 'black';
    });
    josmButtons.forEach(button => {
      button.style.background = "#363636";
      button.style.color = "white";
    });
    idButtons.forEach(button => {
      button.style.background = "#363636";
      button.style.color = "white";
    });
    tileChildren.forEach(tile => {
      tile.classList.remove('has-background-grey-darker');
      tile.classList.add('has-background-white-ter');
    });
    document.querySelectorAll('.arrow-down').forEach(function(icon) {
      icon.src = '../../media/arrow-down.svg';
    });
    document.querySelectorAll('.question-mark').forEach(function(icon) {
      icon.src = '../../media/question-mark.svg';
    });
    document.querySelectorAll('.ext-link').forEach(function(icon) {
      icon.src = '../../media/ext-link-black.svg';
    });
    tabItems.forEach(tabItem => {
      tabItem.classList.remove('has-background-dark'); // Remove dark background from <li>
    });
    tabLinks.forEach(tabLink => {
      tabLink.classList.remove('has-text-light'); // Ensure text color is set correctly for links
      if (tabLink.parentElement.classList.contains('is-active')) {
        // Specifically target the <a> element within the 'is-active' tab
        tabLink.classList.remove('has-background-dark'); // Remove dark background from <a>
      }
    });

    // Update chart
    myChart.options.scales.x.ticks.color = '#4e4949';
    myChart.options.scales.y.ticks.color = '#4e4949';
    myChart.options.plugins.legend.labels.color = '#4e4949';
    myChart.update();

  } else {
    body.setAttribute("data-theme", "dark");
    columnCards.forEach(card => {
      card.classList.add('has-background-grey-darker');
      card.classList.remove('has-background-white-ter'); // Remove the class for light theme
    });
    document.querySelectorAll('.copy-icon').forEach(function(icon) {
      icon.src = '../../media/white-copy-paste.svg';
    });
    document.querySelectorAll('.theme-icon').forEach(function(icon) {
      icon.src = '../../media/white-chart.svg';
    });
    document.querySelectorAll('.website-icon').forEach(function(icon) {
      icon.src = '../../media/white-website.svg';
    });
    document.querySelectorAll('.arrow-icon').forEach(function(icon) {
      icon.src = '../../media/white-arrow-down.svg';
    });
    document.querySelectorAll('.toggle-icon').forEach(function(icon) {
      icon.src = '../../media/theme-dark-light.svg';
    });
    navbarElements.forEach(navbarElement => {
      navbarElement.style.backgroundColor = 'rgb(54, 54, 54)';
    });
    whiteButtons.forEach(button => { // New loop
      button.style.color = 'white';
    });
    document.querySelectorAll('.homepage-icon').forEach(function(icon) {
      icon.src = '../../media/white-homepage.svg';
    });
    titles.forEach(title => {
      title.style.color = 'lightgray';
    });
    table.forEach(table => {
      table.style.backgroundColor = 'lightgray';
    });
    warnings.forEach(warning => {
      warning.style.backgroundColor = '#363636';
      warning.style.color = 'lightgray';
    });
    labelTexts.forEach(labelText => {
      labelText.style.color = 'lightgray';
    });
    labelTextsStrong.forEach(labelText => {
      labelText.style.color = 'lightgray';
    });
    josmButtons.forEach(button => {
      button.style.background = "lightgray"; // Reset to default
      button.style.color = "black"; // Reset to default
    });
    idButtons.forEach(button => {
      button.style.background = "lightgray"; // Reset to default
      button.style.color = "black"; // Reset to default
    });
    tileChildren.forEach(tile => {
      tile.classList.remove('has-background-white-ter');
      tile.classList.add('has-background-grey-darker');
    });
    document.querySelectorAll('.arrow-down').forEach(function(icon) {
      icon.src = '../../media/arrow-down-white.svg';
    });
    document.querySelectorAll('.question-mark').forEach(function(icon) {
      icon.src = '../../media/question-mark-light.svg';
    });
    document.querySelectorAll('.ext-link').forEach(function(icon) {
      icon.src = '../../media/ext-link-white.svg';
    });
    tabItems.forEach(tabItem => {
      tabItem.classList.add('lightgray'); // Add dark background to <li>
    });
    tabLinks.forEach(tabLink => {
      tabLink.classList.add('has-text-light'); // Ensure text color is set correctly for links
      if (tabLink.parentElement.classList.contains('is-active')) {
        // Specifically target the <a> element within the 'is-active' tab
        tabLink.classList.add('has-background-dark'); // Add dark background to <a>
      }
    });

    // Update chart
    myChart.options.scales.x.ticks.color = 'white';
    myChart.options.scales.y.ticks.color = 'white';
    myChart.options.plugins.legend.labels.color = 'white';
    myChart.update();
  }
}