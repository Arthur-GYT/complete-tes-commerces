#!/usr/bin/env python3
import argparse
from main import run_complete_shops
from city_attic_stats import run_city_attic_stats
from update_old_osm_siret import run_update_old_siret
from disused_shops import run_disused_shops


def complete_shops_function(args):
    run_complete_shops(city_name=args.name, city_code=args.code, prevent_update_insee=args.prevent_update_insee)


def city_attic_stats_function(args):
    run_city_attic_stats(city_name=args.name, city_code=args.code)


def update_old_shops_function(args):
    run_update_old_siret(city_name=args.name, city_code=args.code)
    run_disused_shops(city_name=args.name, city_code=args.code)


def main():
    parser = argparse.ArgumentParser(prog='ctc-osm.py')
    subparsers = parser.add_subparsers(help='sub-command help', dest='subcommand')
    subparsers.required = True

    # Define the common arguments for all subparsers
    common_arguments = argparse.ArgumentParser(add_help=False)
    common_arguments.add_argument('-n', '--name', type=str, help='Name of the city')
    common_arguments.add_argument('-c', '--code', type=str, help='INSEE code of the city (5-digit)')

    # Subcommand for main
    parser_main = subparsers.add_parser('main', help='Complete OSM shops missing address/siret',
                                        parents=[common_arguments])
    parser_main.add_argument('--prevent-update-insee', action='store_true', help='Prevent update of INSEE data')
    parser_main.set_defaults(func=complete_shops_function)

    # Subcommand for find_old_shops
    parser_find_old_shops = subparsers.add_parser('find_old_shops', help='Find old shops', parents=[common_arguments])
    parser_find_old_shops.set_defaults(func=update_old_shops_function)

    # Subcommand for city_attic_stats
    parser_city_attic_stats = subparsers.add_parser('city_attic_stats', help='Gather historical tags stats',
                                                    parents=[common_arguments])
    parser_city_attic_stats.set_defaults(func=city_attic_stats_function)

    args = parser.parse_args()
    args.func(args)  # This will call the appropriate function based on the subcommand


if __name__ == '__main__':
    main()
