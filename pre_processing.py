import os
import sys
import zipfile
import datetime
import requests
import base64
import numpy as np
import csv
import urllib3
import pandas as pd
import time
from tqdm import tqdm
from json.decoder import JSONDecodeError
import argparse

urllib3.disable_warnings(urllib3.exceptions.InsecureRequestWarning)


# Get Access Token from SIRENE API
def get_sirene_api_access_token(api_key, api_secret):
    headers = {
        'Content-Type': 'application/x-www-form-urlencoded',
        'Authorization': 'Basic ' + base64.b64encode(f'{api_key}:{api_secret}'.encode()).decode()}
    data = {'grant_type': 'client_credentials'}

    max_retries = 5
    retry_delay = 20  # Starting delay in seconds
    attempt = 0

    while attempt < max_retries:
        try:
            response = requests.post('https://api.insee.fr/token', headers=headers, data=data)
            response.raise_for_status()  # Will raise an HTTPError if HTTP request returned unsuccessful status code

            return response.json()['access_token']
        except (requests.HTTPError, requests.ConnectionError, JSONDecodeError) as e:
            attempt += 1
            print(f"Attempt {attempt} failed: {e}")
            if attempt < max_retries:
                print(f"Requesting a new API token in {retry_delay} seconds...")
                time.sleep(retry_delay)
                retry_delay *= 2  # Double the delay for the next attempt
            else:
                print("Maximum retries reached. Exiting.")
                return None

    # If the loop exits without returning, it means all retries have been exhausted
    return None


def parse_siret_response(answer, dict_build):

    for x in answer.json()['etablissements']:
        unitelegale = x['uniteLegale']['denominationUniteLegale']
        usuelle1unitelegale = x['uniteLegale']['denominationUsuelle1UniteLegale']
        usuelle2unitelegale = x['uniteLegale']['denominationUsuelle2UniteLegale']

        # Concatenate all field names (sometimes the business name is in another field)
        dict_build[x['siret']] = ' '.join(
                set(denomination for denomination in (unitelegale, usuelle1unitelegale, usuelle2unitelegale) if
                    denomination))

    return dict_build


# Get names from a list of SIRETs
def get_siret_and_name_from_api(siret_list, consumer_key, consumer_secret):

    # Convert all SIRET numbers to string and join them with ' OR '
    siret_query = ' OR '.join(str(siret) for siret in siret_list)
    api_url = 'https://api.insee.fr/entreprises/sirene/V3/'
    auth_headers = {'Authorization': 'Bearer ' + get_sirene_api_access_token(consumer_key, consumer_secret)}
    siret_name_dict = {}
    max_retries = 5  # Set a maximum number of retries
    attempt = 0

    while attempt < max_retries:
        try:
            response = requests.get(f'{api_url}siret?q=siret:({siret_query})', headers=auth_headers)
            response.raise_for_status()  # Will raise an HTTPError if HTTP request returned unsuccessful status code
            siret_name_dict = parse_siret_response(response, siret_name_dict)

            content_type = response.headers.get('Content-Type', '').split(';')[0].strip()
            if content_type == 'application/json':
                siret_name_dict.update(parse_siret_response(response, siret_name_dict))
                break  # Exit the loop on success
            else:
                raise ValueError(f"Unexpected content type: {response.headers.get('Content-Type')}")

        # Handling all the Error Codes the INSEE API is throwing at me
        except (JSONDecodeError, ValueError, requests.HTTPError, ConnectionError, ConnectionResetError,
                requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout,
                requests.exceptions.RequestException) as e:
            print(f"Error encountered: {e}. Retrying with a new access token...")
            attempt += 1  # Increment the attempt counter
            time.sleep(10)  # Wait for a short period before retrying

    if attempt == max_retries:
        print("Maximum retries reached. Exiting.")
        sys.exit()

    return siret_name_dict


# Getting city insee code from city name
def get_insee_code_from_city_name(city_name):
    # Updated yearly as this address : https://www.insee.fr/fr/information/6800675

    insee_commune_file = 'data/v_commune_2023.csv'
    df = pd.read_csv(insee_commune_file)
    insee_code = df[df['LIBELLE'] == city_name]['COM'].values[0]

    return insee_code


# Getting city name from city insee code
def get_city_name_from_insee_code(insee_code):
    # Updated yearly as this address : https://www.insee.fr/fr/information/6800675

    insee_commune_file = 'data/v_commune_2023.csv'
    df = pd.read_csv(insee_commune_file)
    city_name = df[df['COM'] == insee_code]['LIBELLE'].values[0]

    return city_name


def separate_opened_closed_sirets(input_csv_file):

    # Classify Open (A) / Closed (F) by field etatAdministratifEtablissement
    column_name = 'etatAdministratifEtablissement'

    # Create an iterator that reads the CSV file in chunks
    chunk_size = 10 ** 5
    chunk_iter = pd.read_csv(input_csv_file, chunksize=chunk_size, dtype=str)

    # Estimate the total number of chunks based on the file size and the chunk size
    total_size = os.path.getsize(input_csv_file)
    total_chunks = total_size // (chunk_size * 180)  # Assuming an average of 200 bytes per row

    # Define the paths for output files
    base_name = os.path.basename(input_csv_file)
    base_name_without_ext = os.path.splitext(base_name)[0]
    output_file_a = os.path.join(os.path.dirname(input_csv_file), base_name_without_ext + '-A.csv')
    output_file_f = os.path.join(os.path.dirname(input_csv_file), base_name_without_ext + '-F.csv')

    # Initialize a variable to keep track of the first chunk and counters for 'A' and 'F'
    first_chunk = True
    count_a = 0
    count_f = 0

    # Iterate over the file in chunks and want to print progress bar with the chunks
    for chunk in tqdm(chunk_iter, total=total_chunks):
        # Filter rows with 'A' and 'F'
        chunk_a = chunk[chunk[column_name] == 'A']
        chunk_f = chunk[chunk[column_name] == 'F']

        # Count the occurrences of 'A' and 'F'
        count_a += len(chunk_a)
        count_f += len(chunk_f)

        # Append filtered rows to separate files
        if first_chunk:
            chunk_a.to_csv(output_file_a, mode='w', index=False)
            chunk_f.to_csv(output_file_f, mode='w', index=False)
            first_chunk = False
        else:
            chunk_a.to_csv(output_file_a, mode='a', index=False, header=False)
            chunk_f.to_csv(output_file_f, mode='a', index=False, header=False)

    # Print the final counts and the ratio
    print("Done")
    print(f"   |---> Total businesses still open: {count_a / 10**6} million(s)")
    print(f"   |---> Total business now closed: {count_f / 10**6} million(s)")


def download_unzip_last_version_sirene_csv():
    today_month_year = datetime.date.today().strftime("%Y-%m")
    sirene_zip_url = "https://www.data.gouv.fr/fr/datasets/r/0651fb76-bcf3-4f6a-a38d-bc04fa708576"
    filepath = f'{os.getcwd()}/data/'
    insee_zip = 'StockEtablissement_utf8.zip'
    insee_file = f'{insee_zip[:-4]}-{today_month_year}.csv'

    # Check if the current month's CSV exists or download it
    if not os.path.exists(filepath + insee_file):
        print(f" |---> Downloading latest Sirene csv file (size ~1.6GB)")
        response = requests.get(sirene_zip_url)
        with open(filepath + insee_zip, 'wb') as f:
            f.write(response.content)

    if os.path.exists(filepath + insee_file):
        print(" |---> Sirene csv file already downloaded")
    else:
        print(" |---> Unzipping Sirene file (this might take a while as well)")
        start = datetime.datetime.now()

        with zipfile.ZipFile(filepath + insee_zip, 'r') as zip_ref:
            extracted_csv_file = zip_ref.namelist()[0]
            zip_ref.extractall(filepath)

            old_file_path = os.path.join(filepath, extracted_csv_file)
            new_file_path = os.path.join(filepath, extracted_csv_file.replace('.csv', f'-{today_month_year}.csv'))

            os.rename(old_file_path, new_file_path)

        print(" |---> Separating open/closed businesses")
        # Separate the downloaded files into 2 files (open/closed shops)
        separate_opened_closed_sirets(filepath + insee_file)

        end = datetime.datetime.now()
        timelapse_min, timelapse_sec = elapsed_time(start, end)
        print(f"...Done in {int(timelapse_min)} min and {int(timelapse_sec)} sec")


# Filter out City Siret closed or not checked recently
def filter_out_old_not_checked_businesses(csv_file_input, year_thrsld):

    print(" |---> Filtering out irrelevant businesses (too old or not checked in a while)", end="", flush=True)

    # Load the CSV file
    dtype_dict = {'siret': str, 'nic': str}
    df = pd.read_csv(csv_file_input, dtype=dtype_dict, low_memory=False)

    # Prepare a mask for 'etatAdministratifEtablissement' field equals to 'A'
    etat_administratif_mask = df['etatAdministratifEtablissement'] == 'A'

    # Mask for 'dateDernierTraitementEtablissement' less than 9 year old
    df['dateDernierTraitementEtablissement'] = pd.to_datetime(df['dateDernierTraitementEtablissement'], errors='coerce')
    checked_date_mask = df['dateDernierTraitementEtablissement'] > pd.Timestamp.now() - pd.DateOffset(years=year_thrsld)

    # Apply both masks (REMOVED empty_fields_mask)
    cleandf = df[etat_administratif_mask & checked_date_mask]
    print("...Done")

    return cleandf


#  Download additional data from Sirene API
def download_additional_data_from_api(filtered_df, csv_file_output, consumer_key, consumer_secret):

    if consumer_key is None or consumer_secret is None:
        with open(csv_file_output, 'w', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(['siret', 'name'])
        print(" |---> Skipping additional data from Sirene API")
    else:
        print(" |---> Downloading additional data from Sirene API")

        # Specify the CSV file's headers
        headers = ['siret', 'name']

        # Open the CSV file for appending
        with open(csv_file_output, 'a') as f:
            writer = csv.DictWriter(f, fieldnames=headers)

            # Write the headers if the file is empty
            if f.tell() == 0:
                writer.writeheader()

            # Define batch size and calculate total number of batches for Sirene API
            batch_size = 20
            total_batches = len(filtered_df) // batch_size
            if len(filtered_df) % batch_size != 0:
                total_batches += 1

            # Process the DataFrame in batches
            for i, (_, batch) in enumerate(filtered_df.groupby(np.arange(len(filtered_df)) // batch_size), start=1):

                # Get the 'siret' values as a list
                siret_list = batch['siret'].tolist()

                # Get the data from API
                siret_name_dict = get_siret_and_name_from_api(siret_list, consumer_key, consumer_secret)

                # Iterate over dictionary items and write each as a row in the CSV file
                for siret, name in siret_name_dict.items():
                    writer.writerow({'siret': siret, 'name': name})

                # Calculate the percentage of progress
                progress_percentage = (i / total_batches) * 100
                print(f'\r |---> Progress: {progress_percentage:.1f}%', end='')

        print(f'\r |---> Progress: 100%...Done')


# Merge API Names Into Siret CSV
def merge_api_names_into_siret_csv(csv_file_main, csv_file_api_extracted, merge_file_output):
    # Read the csv files
    df1 = pd.read_csv(csv_file_main,
                      dtype={'siret': str, 'siren': str, 'nic': str, 'codeCommuneEtablissement': str})
    df2 = pd.read_csv(csv_file_api_extracted, dtype={'siret': str})

    # remove extra whitespaces in 'name' column
    df2['name'] = df2['name'].str.replace(r'\s+', ' ', regex=True)

    # merge on 'siret'
    merged_df = pd.merge(df1, df2, on='siret', how='left')

    # Add 'complementAdresseEtablissement' to 'enseigne1Etablissement' (additional info for business names)
    merged_df['name'] = merged_df['name'].fillna('')
    merged_df['complementAdresseEtablissement'] = merged_df['complementAdresseEtablissement'].fillna('')
    merged_df['name'] = merged_df.apply(lambda row: str(row['name']) + ' ' + str(row['complementAdresseEtablissement'])
                                    if pd.notnull(row['complementAdresseEtablissement']) else str(row['name']), axis=1)

    # Replace NaN values in 'enseigne1Etablissement' with an empty string
    merged_df['enseigne1Etablissement'] = merged_df['enseigne1Etablissement'].fillna('')

    # add 'name' to 'enseigne1Etablissement' when 'name' is not null & drop 'name' column
    merged_df.loc[merged_df['name'].notna(), 'enseigne1Etablissement'] = merged_df.loc[merged_df['name'].notna(),
                                     'enseigne1Etablissement'] + " " + merged_df.loc[merged_df['name'].notna(), 'name']

    merged_df = merged_df.drop('name', axis=1)

    # write to a new csv file
    merged_df.to_csv(merge_file_output, index=False)


def extract_city_from_whole_csv_file(filepath, city_insee_code, insee_filepath, chunk_size):

    today_date = datetime.date.today().strftime("%Y-%m")
    first_one = True

    for chunk in pd.read_csv(insee_filepath, chunksize=chunk_size, dtype=str):
        df_city_insee = chunk[chunk['codeCommuneEtablissement'] == city_insee_code]
        if not df_city_insee.empty:
            if first_one:  # if it's the first chunk include the csv headers, otherwise no
                df_city_insee.to_csv(f'{filepath}{city_insee_code}-{today_date}.csv', mode='a', index=False)
                first_one = False
            else:
                df_city_insee.to_csv(f'{filepath}{city_insee_code}-{today_date}.csv', mode='a', index=False,
                                     header=False)


def elapsed_time(start_time, end_time):

    elapsed = end_time - start_time
    total_seconds = elapsed.total_seconds()
    minutes = total_seconds // 60
    seconds = total_seconds % 60

    return minutes, seconds


def get_downloaded_identifiers(csv_file):
    downloaded_df = pd.read_csv(csv_file, dtype=str)
    # Assuming 'siret' is the column name in the downloaded CSV file
    downloaded_ids = set(downloaded_df['siret'])
    return downloaded_ids


def get_missing_siret_not_downloaded(filtered_df, downloaded_csv):
    original_ids = set(filtered_df['siret'])
    downloaded_ids = get_downloaded_identifiers(downloaded_csv)
    missing_ids = original_ids - downloaded_ids
    # Filter the DataFrame to return only rows with missing siret
    missing_siret_df = filtered_df[filtered_df['siret'].isin(missing_ids)]

    return missing_siret_df


def extract_city_businesses_from_sirene(city_insee_code, year_threshold):

    consumer_key = os.getenv('SIRENE_API_KEY')
    consumer_secret = os.getenv('SIRENE_API_SECRET')

    if not consumer_key or not consumer_secret:
        raise ValueError("SIRENE API credentials not found in the environment variables.")

    # today = datetime.date.today().strftime("%Y-%m-%d")
    today_month_year = datetime.date.today().strftime("%Y-%m")
    filepath = f'{os.getcwd()}/data/'
    insee_file = f'{filepath}StockEtablissement_utf8-{today_month_year}-A.csv'
    csv_filename = f'{filepath}{city_insee_code}-{today_month_year}.csv'
    sirene_api_dataset_csv = f'{filepath}sirene_api_dataset-{city_insee_code}-{today_month_year}.csv'
    sirene_api_conflated_file = f'{filepath}working_{city_insee_code}-{today_month_year}.csv'
    chunksize = 10 ** 6  # no more than 6 for my laptop

    if os.path.exists(f"{filepath}{city_insee_code}-{today_month_year}.csv"):
        print(f" |---> Data already extracted from Sirene file")
    else:
        print(f" |---> Extracting city data (please wait)", end="", flush=True)
        start = datetime.datetime.now()

        extract_city_from_whole_csv_file(filepath, city_insee_code, insee_file, chunksize)

        end = datetime.datetime.now()
        timelapse_min, timelapse_sec = elapsed_time(start, end)
        print(f"...Done in {int(timelapse_min)} min and {int(timelapse_sec)} sec")

    #############################################
    #   POPULATING AND CLEANING UP SIRET FILE   #
    #############################################

    if os.path.exists(sirene_api_conflated_file):
        print(f" |---> Sirene API data already downloaded/merged")
    else:
        if os.path.exists(sirene_api_dataset_csv):
            # If we are here, there was a problem, file should have been deleted last run, let's get missing items
            filtered_df = filter_out_old_not_checked_businesses(csv_filename, year_threshold)
            df_sirets_miss_dl = get_missing_siret_not_downloaded(filtered_df, sirene_api_dataset_csv)
            download_additional_data_from_api(df_sirets_miss_dl, sirene_api_dataset_csv, consumer_key, consumer_secret)
        else:
            # All good, let's just download from scratch
            filtered_df = filter_out_old_not_checked_businesses(csv_filename, year_threshold)
            download_additional_data_from_api(filtered_df, sirene_api_dataset_csv, consumer_key, consumer_secret)

        print(" |---> Merging downloaded data into main Sirene csv file", end="", flush=True)
        merge_api_names_into_siret_csv(csv_filename, sirene_api_dataset_csv, sirene_api_conflated_file)
        print('...Done')

    print(" |---> Cleaning up temp files", end="", flush=True)
    if os.path.exists(f'{filepath}StockEtablissement_utf8.zip'):
        os.remove(f'{filepath}StockEtablissement_utf8.zip')
    if os.path.exists(f'{sirene_api_dataset_csv}'):
        os.remove(f'{sirene_api_dataset_csv}')
    # TODo: Exchange name working and not working csv file. delete working one (pre-api)
    print("...Done")


if __name__ == "__main__":
    # Create the parser
    parser = argparse.ArgumentParser(description='Extract city businesses from SIRENE.')

    # Add arguments to the parser
    parser.add_argument('city_insee_code', type=str, help='City INSEE code')
    parser.add_argument('year_threshold', type=int, help='Year threshold for filtering businesses')

    # Parse the command-line arguments
    args = parser.parse_args()

    # Call the function with the parsed arguments and the environment variables for API credentials
    extract_city_businesses_from_sirene(args.city_insee_code, args.year_threshold)
