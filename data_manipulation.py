import pandas as pd
from query_pois import get_overpass_response
import re


# Replace ';' with ',' in the overpass answer
def format_overpass_answer_to_json(dirty_json):

    tags_field_names = [list(item['tags'].keys()) for item in dirty_json]
    unique_field_names = list(set([field for sublist in tags_field_names for field in sublist]))

    for item in dirty_json:
        # Split potential_streets on semicolon to create a list
        item['tags']['potential_streets'] = item['tags']['potential_streets'].split(';')

        # Handle potential_addresses separately due to the possibility of empty items
        potential_addresses1 = item['tags']['potential_addresses'].replace("'", "\'")
        potential_addresses = potential_addresses1.split(';')
        item['tags']['potential_addresses'] = [(address.split('|')[0].strip(), address.split('|')[1].strip())
                                               if '|' in address else (address.strip(), '')
                                               for address in potential_addresses]
    json_data = [item['tags'] for item in dirty_json]

    return json_data, unique_field_names


def process_df_streets_with_minmax_housenumber(json_item, columns):

    df = pd.DataFrame(json_item, columns=columns)
    df['housenum'] = None

    for i, row in df.iterrows():
        matched_street_addr = []
        if row['potential_streets'] and row['potential_streets'][0] != '':
            for street in row['potential_streets']:
                housenums = []
                if row['potential_addresses']:
                    for address in row['potential_addresses']:
                        if street == address[1] and address[0]:
                            housenum = re.sub(r'\D', '', address[0])  # Remove non-numerical characters
                            if housenum:  # Check if housenum is not empty after removing non-numerical characters
                                housenums.append(int(housenum))
                    if housenums:
                        min_max_housenum = (max(1, min(housenums) - 10), max(housenums) + 10)
                        matched_street_addr.append([street, min_max_housenum])
                    else:
                        matched_street_addr.append([street, None])

        df.at[i, 'housenum'] = matched_street_addr if matched_street_addr else [None, None]

    # Check if the columns exist before trying to drop them
    if 'potential_streets' in df.columns and 'potential_addresses' in df.columns:
        df.drop(columns=['potential_streets', 'potential_addresses'], inplace=True)
    else:
        pass

    return df


def get_df_pois_missing_address(city, dict_osm_pois_tags, distance_to_poi):

    dirtyjson = get_overpass_response('\"' + city + '\"', distance_to_poi, dict_osm_pois_tags)
    clean_json, osm_tags = format_overpass_answer_to_json(dirtyjson)

    df_pois_missing_address = process_df_streets_with_minmax_housenumber(clean_json, osm_tags)

    return df_pois_missing_address
