from pre_processing import get_sirene_api_access_token, get_insee_code_from_city_name, get_city_name_from_insee_code
from frontend import create_bulma_theme
import overpass
from datetime import datetime, timedelta, date
import pandas as pd
import os
from dotenv import load_dotenv
import requests
import time
import sys
import json
from collections import OrderedDict
import re


# Get environment variables
load_dotenv()
sirene_api_key = os.getenv('SIRENE_API_KEY')
sirene_api_secret = os.getenv('SIRENE_API_SECRET')


def build_overpass_query_pois_with_siret(city, tags_dictionnary):

    tags_query_string = "(\n"
    for poi_tag, value in tags_dictionnary.items():
        whitelist = "|".join(value["whitelist"])
        blacklist = "|".join(value["blacklist"])

        whitelist_string = f'["{poi_tag}"~"{whitelist}"]' if whitelist else ''
        blacklist_string = f'["{poi_tag}"!~"{blacklist}"]' if blacklist else ''

        tags_query_string += (f'nw["{poi_tag}"]{whitelist_string}{blacklist_string}'
                              f'["ref:FR:SIRET"](area.france)(area.ville);\n')

    tags_query_string += ");"

    # Building complete overpass query
    query = f'''
[out:json][timeout:300];
area["admin_level"="2"]["name"="France"]->.france;
area["admin_level"="8"]["name"="{city}"]->.ville;

{tags_query_string}
  out geom;
    '''

    return query


def get_overpass_response(city, dict_osm_pois):
    query_overpass = build_overpass_query_pois_with_siret(city, dict_osm_pois)

    # Execute overpass query
    api = overpass.API(timeout=100)
    response_api = api.Get(query_overpass, responseformat=f"json", verbosity='geom', build=False)

    return response_api['elements']


def batch_api_download(siret_data, consumer_key, consumer_secret):
    if consumer_key is None or consumer_secret is None:
        print("    Skipping additional data from Sirene API")
        return

    # Initialize a dictionary to store the API results
    api_results = {}

    # Process the SIRETs
    for closed_siret, siret_info in siret_data.items():
        open_sirets = siret_info['active_sirets']
        # Check if there are more than 200 open SIRETs for the closed SIRET
        if len(open_sirets) > 200:
            continue

        # Get the data for the closed SIRET
        closed_siret_data = download_api_siret_data([closed_siret], consumer_key, consumer_secret)
        if closed_siret in closed_siret_data:
            # Initialize the entry for the closed SIRET with its own data
            api_results[closed_siret] = {'osm_name': siret_info['osm_name'],
                                         'osm_type': siret_info['osm_type'],
                                         'osm_id': siret_info['osm_id'],
                                         'osm_lat': siret_info['osm_lat'],
                                         'osm_lon': siret_info['osm_lon'],
                                         'poi_website': siret_info['poi_website'],
                                         'denomination': closed_siret_data[closed_siret]['denomination'],
                                         'complement_address': closed_siret_data[closed_siret]['complement_address'],
                                         'code_naf': closed_siret_data[closed_siret]['code_naf'],
                                         'last_change_date': closed_siret_data[closed_siret]['last_change_date'],
                                         'open_sirets': []
                                         }
        else:
            # If no data was found for the closed SIRET, skip to the next one
            continue

        # Define batch size for Sirene API
        batch_size = 10
        total_batches = len(open_sirets) // batch_size + (len(open_sirets) % batch_size > 0)

        # Process the open SIRETs in batches
        for i in range(total_batches):
            start_index = i * batch_size
            end_index = start_index + batch_size
            batch_sirets = open_sirets[start_index:end_index]

            # Get the data from API
            siret_dict = download_api_siret_data(batch_sirets, consumer_key, consumer_secret)
            for siret in batch_sirets:
                if siret in siret_dict:
                    api_results[closed_siret]['open_sirets'].append({siret: siret_dict[siret]})

        # Print progress
        progress_percentage = (len(api_results) / len(siret_data)) * 100
        print(f'\r        Progress: {progress_percentage:.1f}%', end='')

    print(f'\r        Progress: 100%...Done')

    return api_results


def download_api_siret_data(siret_list, consumer_key, consumer_secret):

    # Convert all SIRET numbers to string and join them with ' OR '
    siret_dict = {}
    siret_query = ' OR '.join(str(siret) for siret in siret_list)
    api_url = 'https://api.insee.fr/entreprises/sirene/V3/'
    auth_headers = {'Authorization': 'Bearer ' + get_sirene_api_access_token(consumer_key, consumer_secret)}

    try:
        response2 = requests.get(f'{api_url}siret?q=siret:({siret_query})', headers=auth_headers)
        # Assuming the API response is a JSON object with a list of establishments
        establishments = response2.json().get('etablissements', [])

        # Loop through each establishment and extract the relevant data
        for establishment in establishments:
            # Extract SIRET and other desired fields
            siret = establishment.get('siret')
            unite_legale = establishment.get('uniteLegale', {})

            name_unite = unite_legale.get('denominationUniteLegale') or ''
            denomination_usuelle = unite_legale.get('denominationUsuelle1UniteLegale', '') or ''
            name = ' '.join(filter(None, [name_unite.strip(), denomination_usuelle.strip()]))
            code_naf = unite_legale.get('activitePrincipaleUniteLegale')
            complement_address = establishment.get('adresseEtablissement', {}).get('complementAdresseEtablissement')
            create_date = unite_legale.get('dateCreationUniteLegale')

            # Access the 'periodesEtablissement' list
            periodes = establishment.get('periodesEtablissement', [])

            # If there are periods available, extract data from most recent one
            if periodes:
                most_recent_periode = periodes[0]  # Get the most recent period
                last_change_date = most_recent_periode.get('dateDebut')
                derniere_periode = most_recent_periode.get('etatAdministratifEtablissement')

                enseigne = most_recent_periode.get('enseigne1Etablissement') or ''
                designation = most_recent_periode.get('denominationUsuelleEtablissement') or ''
                denomin = ' '.join(filter(None, [name.strip(), enseigne.strip(), designation.strip()])).split()
                unique_denomin = list(OrderedDict.fromkeys(denomin))
                denomination = ' '.join(unique_denomin)  # We merged a bunch of name fields into 1 and keep unique words
            else:
                last_change_date = ''
                denomination = ''
                derniere_periode = ''

            # Add the data to the dictionary
            siret_dict[siret] = {
                'denomination': denomination,
                'complement_address': complement_address,
                'code_naf': code_naf,
                'create_date': create_date,
                'last_change_date': last_change_date,
                'dernier_periode_fermé': derniere_periode,
            }

        return siret_dict

    except (
        ConnectionError, ConnectionResetError, requests.exceptions.ConnectionError, requests.exceptions.ReadTimeout
            ) as e:
        # Retry logic
        for _ in range(5):
            print(f"\nAPI refused the connection: {e}. Retrying...")
            time.sleep(10)
            try:
                response2 = requests.get(f'{api_url}siret?q=siret:({siret_query})', headers=auth_headers)
                # If successful, break the retry loop
                break
            except (ConnectionError, ConnectionResetError, requests.exceptions.ConnectionError,
                    requests.exceptions.ReadTimeout):
                continue
        else:
            # If all retries failed, exit the program
            print("All retries failed. Exiting the program.")
            sys.exit()
    except requests.exceptions.RequestException as e:
        print(f"Connection Error: {e}")
        sys.exit()

    return response2


# Gather POI website tags
def get_priority_website(tags):
    # Order of priority for the website-related tags
    website_tags = [
        'contact:website', 'website',
        'contact:facebook', 'facebook',
        'contact:instagram', 'instagram'
    ]

    # Check for each tag in the order of priority and return the first found
    for tag in website_tags:
        if tag in tags:
            return tags[tag]
    return ''  # Return empty string if none of the tags are found


def get_new_siret_for_expired_siret(response, df_open_sirene_csv, df_closed_siret):

    # Dictionary to hold active SIRETs grouped by the (street, voienum) of closed SIRETs
    active_sirets_by_closed_siret = {}

    # Loop through each item in the response to find closed 'F' SIRETs
    for item in response:
        if 'tags' in item and 'ref:FR:SIRET' in item['tags']:
            siret_to_check = item['tags']['ref:FR:SIRET']
            closed_siret_info = df_closed_siret.loc[df_closed_siret['siret'] == siret_to_check]

            if not closed_siret_info.empty:
                street = closed_siret_info['libelleVoieEtablissement'].iloc[0]
                voienum = closed_siret_info['numeroVoieEtablissement'].iloc[0]
                osm_name = item['tags'].get('name', '')
                osm_type = item.get('type', '')
                osm_id = item.get('id', '')
                osm_lat = item.get('lat', '')
                osm_lon = item.get('lon', '')
                poiwebsite = get_priority_website(item['tags'])

                # Find all 'A' SIRETs at the same location
                active_sirets_same_location = df_open_sirene_csv[
                    (df_open_sirene_csv['libelleVoieEtablissement'] == street) &
                    (df_open_sirene_csv['numeroVoieEtablissement'] == voienum)
                ][['siret']]

                # If there are active SIRETs at the same location, add them to the dictionary
                if not active_sirets_same_location.empty:
                    active_sirets_by_closed_siret[siret_to_check] = {
                        'osm_name': osm_name,
                        'osm_type': osm_type,
                        'osm_id': osm_id,
                        'osm_lat': osm_lat,
                        'osm_lon': osm_lon,
                        'poi_website': poiwebsite,
                        'active_sirets': active_sirets_same_location['siret'].astype(str).tolist()
                    }
    return active_sirets_by_closed_siret


# Function to calculate if the create_date is within the last 12 months of the reference change_date
def is_within_last_15_years(create_date_str, reference_change_date_str):
    create_date = datetime.strptime(create_date_str, "%Y-%m-%d")
    reference_change_date = datetime.strptime(reference_change_date_str, "%Y-%m-%d")
    date_difference = (create_date - reference_change_date).days
    return date_difference >= -5500


# Filter out SIRET numbers based on the create_date condition
def filter_json_siret(json_input):

    filtered_data = {}
    useless_naf_codes = {"77.40Z", "68.20A", "68.20B", "70.10Z", "66.19A"}  # SCI type or landowner businesses

    for main_siret, main_data in json_input.items():
        reference_change_date_str = main_data["last_change_date"]
        filtered_sirets = []

        for open_siret_dict in main_data.get("open_sirets", []):
            for siret_number, siret_data in open_siret_dict.items():
                if (is_within_last_15_years(siret_data["create_date"], reference_change_date_str)
                        and siret_data["denomination"].strip()
                        and siret_data["code_naf"] not in useless_naf_codes):
                    filtered_sirets.append({siret_number: siret_data})

        if filtered_sirets:
            filtered_data[main_siret] = main_data.copy()
            filtered_data[main_siret]["open_sirets"] = filtered_sirets

    return filtered_data


# Define a custom sort function
def custom_sort(item, parent_code_naf, parent_denomination, parent_last_change_date):
    siret_info = list(item.values())[0]  # Extract the dictionary from the single-item list
    create_date = datetime.strptime(siret_info['create_date'], "%Y-%m-%d")
    last_change_date = datetime.strptime(parent_last_change_date, "%Y-%m-%d")
    code_naf = siret_info['code_naf'][:-1]

    # Calculate the importance based on the criteria
    same_date = create_date == last_change_date
    within_30_days = abs((create_date - last_change_date).days) <= 30
    same_code_naf = code_naf == parent_code_naf
    same_first_two_digits = code_naf[:2] == parent_code_naf[:2]

    # Check for denomination words in the fields
    denomination_words = set(parent_denomination.upper().split())
    child_fields_words = set(
        (siret_info['denomination'] + ' ' + (siret_info['complement_address'] or '')).upper().split())
    contains_words = not denomination_words.isdisjoint(child_fields_words)

    # Return a tuple with the sorting keys
    return (
        not same_date,  # False (0) if same, which takes priority in sorting
        not within_30_days,  # False (0) if within 30 days
        not same_code_naf,  # False (0) if same code_naf
        not same_first_two_digits,  # False (0) if same first two digits
        not contains_words,  # False (0) if contains words from denomination
        -create_date.timestamp()  # Negative timestamp to sort by newest first
    )


def sort_newly_opened_siret(json_data):

    # Sort the open_sirets for each parent siret
    for parent_siret, parent_info in json_data.items():
        parent_code_naf = parent_info['code_naf'][:-1]
        parent_denomination = parent_info['denomination']
        parent_last_change_date = parent_info['last_change_date']
        parent_info['open_sirets'].sort(
            key=lambda item: custom_sort(item, parent_code_naf, parent_denomination, parent_last_change_date))

    # Let's get parent sirets with fewer open_sirets first
    siret_items = list(json_data.items())
    siret_items.sort(
        key=lambda item: (
            len(item[1]['open_sirets']),  # First sort by number of open sirets then by number of matching NAF codes
            -sum(1 for opsir in item[1]['open_sirets'] if list(opsir.values())[0]['code_naf'] == item[1]['code_naf']),
            len(item[1]['denomination'])   # Then sort by the length of the denomination
        ),
        reverse=False
    )
    json_data = dict(siret_items)

    return json_data


def word_in_denomination(word, denomination):
    return re.search(r'\b' + re.escape(word) + r'\b', denomination, flags=re.IGNORECASE) is not None


def determine_match(parent, children):
    parent_denomination = parent.get('denomination', '').lower()
    parent_code_naf = parent.get('code_naf')
    parent_last_change_date = parent.get('last_change_date')

    # Convert date strings to datetime objects for comparison
    parent_last_change_date = datetime.strptime(parent_last_change_date, '%Y-%m-%d')

    # Initialize flags to track if any child meets the criteria
    no_survey_needed_flag = False
    same_shop_type_probable_name_change_flag = False

    if len(children) == 1:
        child = list(children[0].values())[0]
        child_denomination = child.get('denomination', '').lower()
        child_code_naf = child.get('code_naf')
        child_create_date = child.get('create_date')

        # Convert date strings to datetime objects for comparison
        child_create_date = datetime.strptime(child_create_date, '%Y-%m-%d')

        # Check if parent denomination is in child denomination
        if any(word_in_denomination(word, child_denomination) for word in parent_denomination.split()) or \
                (parent_code_naf[:-1] == child_code_naf[:-1] and parent_last_change_date == child_create_date):
            return 'no_survey_needed'

        if parent_code_naf == child_code_naf:
            return 'same_shop_type_possible_name_change'

    elif len(children) > 1:
        for child in children:
            child_data = list(child.values())[0]
            child_denomination = child_data.get('denomination', '').lower()
            child_code_naf = child_data.get('code_naf')
            child_create_date = datetime.strptime(child_data.get('create_date'), '%Y-%m-%d')

            # Check if at least one child has the same code_naf as the parent
            # and the create_date is within ±8 months of the parent's last_change_date
            if (child_code_naf[:-1] == parent_code_naf[:-1] and
                    (parent_last_change_date - timedelta(days=8 * 30) <= child_create_date <= parent_last_change_date
                    + timedelta(days=8 * 30))):
                if any(word_in_denomination(word, child_denomination) for word in parent_denomination.split()) and \
                        (parent_last_change_date - timedelta(days=3) <= child_create_date <= parent_last_change_date
                         + timedelta(days=3)):
                    no_survey_needed_flag = True
                    break  # Found a match, no need to check further children
                else:
                    same_shop_type_probable_name_change_flag = True

        # Return 'no_survey_needed' if any child meets the criteria
        if no_survey_needed_flag:
            return 'no_survey_needed'
            # Return 'same_shop_type_probable_name_change' if applicable and no child met 'no_survey_needed'
        elif same_shop_type_probable_name_change_flag:
            return 'same_shop_type_possible_name_change'

    return 'more_complex_case'  # Default case if none of the conditions are met


def add_match_field_to_json(json_data):
    for siret, data in json_data.items():
        match = determine_match(data, data['open_sirets'])
        data['match'] = match
    return json_data


def extract_city_from_whole_csv_file(file_path, city_insee_code, insee_filepath, chunk_size):

    today_date = date.today().strftime("%Y-%m")
    first_one = True

    for chunk in pd.read_csv(insee_filepath, chunksize=chunk_size, dtype=str):
        df_city_insee = chunk[chunk['codeCommuneEtablissement'] == city_insee_code]
        if not df_city_insee.empty:
            if first_one:  # if it's the first chunk include the csv headers, otherwise no
                df_city_insee.to_csv(f'{file_path}closed_{city_insee_code}-{today_date}.csv', mode='a', index=False)
                first_one = False
            else:
                df_city_insee.to_csv(f'{file_path}closed_{city_insee_code}-{today_date}.csv', mode='a', index=False,
                                     header=False)


def create_old_siret_frontend(city):

    city_lowered = city.lower().replace(" ", "-")
    html_content = create_bulma_theme(city)

    html_content += f"""
<body>
<section class="section is-title-bar py-4">
  <div class="level">
    <!-- Logo and home link -->
    <div class="level-left">
      <div class="level-item">
        <a href="https://www.complete-tes-commerces.fr">
          <img src="../../media/homepage.svg" style="width: 25px; height: auto;" 
          class="homepage-icon" alt="Complete Tes Commerces Logo">
        </a>
      </div>
      <div class="level-item">
        <h1 class="title is-size-4 pr-6" style="color: black;">
          / {city}
        </h1>
      </div>
      <!-- Tab navigation -->
      <div class="level-item">
        <div class="tabs is-boxed">
          <ul>
            <li><a href="index.html">Ajout SIRET/Adresse</a></li>
            <li class="is-active"><a href="siret_clos.html">SIRETs Clôturés</a></li>
            <li><a href="commerces_vides.html">Commerces Désaffectés</a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section">
    <div class=".container.is-widescreen">
        <!-- Question mark icon with tooltip -->
        <div class="help-icon-container" style="display: flex; justify-content: space-between; 
                                                align-items: center; margin: 20px 0;">
            <div style="flex-grow: 1; text-align: center;">
                <!-- The osmLink will be inserted here in the center -->
            </div>
            <div class="tooltip" style="flex-shrink: 0;">
                <img src="../../media/question-mark.svg" alt="Help" class="question-mark" style="vertical-align: middle; 
                                                transform: scale(1.5); transform-origin: center;">
                <span class="tooltiptext has-text-left">
                    - Recherche de nouveaux SIRETs pour les Commerces OSM dont le SIRET a expiré. Les propositions
                    sont à la même adresse et retrouvées grâce à la base de données ouvertes Sirene de l'INSEE.<br><br>
                    - Une catégorisation des changements est tentée mais sans garantie, il est très vivement recommandé
                    de vérifier sur le terrain car certains changement de SIRET sont seulement administratifs.
                </span>
            </div>
        </div>
        
        <!-- Content for the "SIRET Expiré" tab -->
        <!-- The content below should be on the page that "your-siret-expire-url" points to -->
        <div id="tile-container">
            <div class="block">
                <hr class="divider">
                <h2 class="title is-5">- Mise à jour administrative (pas de changement de nom) :</h2>
                <div id="easy-container" class="tiles-container"></div>
            </div>

            <div class="block">
                <hr class="divider">
                <h2 class="title is-5">- Commerces potentiellement remplacés :</h2>
                <div id="unsure-container" class="tiles-container"></div>
            </div>

            <div class="block">
                <hr class="divider">
                <h2 class="title is-5">- Autres :</h2>
                <div id="unknown-container" class="tiles-container"></div>
            </div>
        </div>
    </div>
</section>
  <script>
  window.closedSirets = `../../ville/{city_lowered}/json/{city_lowered}_osm_closed_siret.json`;
  window.disusedPOIs = `../../ville/{city_lowered}/json/{city_lowered}_osm_disused_shops.json`;
  </script>
<script src="../../js/closed_pois.js"></script>
<script src="../../js/toggle_theme.js"></script>
</body>
</html>
    """

    with open(f'website/ville/{city_lowered}/siret_clos.html', "w") as old_siret_file:
        old_siret_file.write(html_content)


def run_update_old_siret(city_name=None, city_code=None):

    today_month_year = date.today().strftime("%Y-%m")

    if city_name:
        try:
            city_code = get_insee_code_from_city_name(city_name)
        except IndexError:
            sys.exit(f"Error: No Insee City Code found for {city_name}, please check city name again "
                     f"in data/v_commune_2023.csv.")
        except Exception as e:
            sys.exit(f"Error: {str(e)}")
    elif city_code:
        city_name = get_city_name_from_insee_code(city_code)

    city_lowered = city_name.lower().replace(" ", "-")
    json_directory = f'website/ville/{city_lowered}/json/'

    # Check if the directory exists, and if not, create it
    if not os.path.exists(json_directory):
        os.makedirs(json_directory)

    json_file_siret_complete = os.path.join(json_directory, f'{city_lowered}_osm_closed_siret.json')
    chunksize = 10 ** 4
    filepath = f'{os.getcwd()}/data/'
    insee_file = f'{filepath}StockEtablissement_utf8-{today_month_year}-F.csv'
    csvfile_sirene = f'data/working_{city_code}-{today_month_year}.csv'
    csvfile_closed_sirene = f'data/closed_{city_code}-{today_month_year}.csv'

    dict_osm_pois_tags = {
        "shop": {
            "whitelist": [],
            "blacklist": ["kiosk", "vacant"]
        },
        "amenity": {
            "whitelist": ["pub", "bar", "cafe", "restaurant", "fast_food", "nightclub", "bank", "ice_cream",
                          "car_rental", "stripclub", "theatre"],
            "blacklist": ["public_bookcase", 'public_building']
        }
    }

    print(f"Getting closed down SIRET in OSM for {city_name}")
    print(f"  |---> Retrieving overpass siret data")
    # Retrieve the overpass response using your functions
    overpass_response = get_overpass_response(city_name, dict_osm_pois_tags)

    print("  |---> Loading up all open and closed sirets")
    # Load the working CSV file into a pandas DataFrame (open businesses)
    df_sirene = pd.read_csv(csvfile_sirene)

    # Load closed businesses (F)
    if not os.path.exists(csvfile_closed_sirene):
        print("  |---> Need to extract closed businesses from Sirene csv (please wait)")
        extract_city_from_whole_csv_file(filepath, city_code, insee_file, chunksize)
    df_closed_sirene = pd.read_csv(csvfile_closed_sirene, dtype={'siret': str})

    print("  |---> Gathering closed siret and new siret at the same address")
    list_of_new_siret_with_closed_ones = get_new_siret_for_expired_siret(overpass_response, df_sirene, df_closed_sirene)

    print("  |---> Downloading additional data from Sirene API")
    downloaded_json = batch_api_download(list_of_new_siret_with_closed_ones, sirene_api_key, sirene_api_secret)

    print("  |---> Filtering out data")
    filtered_json = filter_json_siret(downloaded_json)

    print("  |---> Sorting results and adding match status")
    sorted_json = sort_newly_opened_siret(filtered_json)
    sorted_json = add_match_field_to_json(sorted_json)

    # Save the sorted JSON back to a file
    with open(json_file_siret_complete, 'w') as file:
        json.dump(sorted_json, file, indent=4)

    print("  |---> Creating frontend for closed POIs")
    create_old_siret_frontend(city_name)


def validate_city_insee_code(code):
    if len(code) != 5 or not (code[0].isdigit() and code[2:].isdigit() and (code[1].isdigit() or code[1].isalpha())):
        raise argparse.ArgumentTypeError("City code must be 5 digits")
    return code


if __name__ == "__main__":
    import argparse
    parser = argparse.ArgumentParser(description='Gather closed down SIRETs from OSM')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-n', '--name', type=str, help='the name of the city')
    group.add_argument('-c', '--code', type=validate_city_insee_code, help='the city code (5-digit)')
    args = parser.parse_args()

    if args.name:
        run_update_old_siret(city_name=args.name)
    elif args.code:
        run_update_old_siret(city_code=args.code)
