from dotenv import load_dotenv
from pre_processing import (extract_city_businesses_from_sirene, download_unzip_last_version_sirene_csv,
                            get_insee_code_from_city_name, get_city_name_from_insee_code)
from query_pois import get_count_total_pois_city
from data_manipulation import get_df_pois_missing_address
from check_sirene_db import get_all_good_sirene_matches
from minimap import process_osm_tiles
from frontend import process_json_for_frontend, create_city_frontend, build_dashboard_frontend
from logger import logger
import argparse
import datetime
import json
import sys
import os


def validate_city_insee_code(code):
    if len(code) != 5 or not (code[0].isdigit() and code[2:].isdigit() and (code[1].isdigit() or code[1].isalpha())):
        raise argparse.ArgumentTypeError("City code must be 5 digits")
    return code


def run_complete_shops(city_name=None, city_code=None, prevent_update_insee=False):
    # Get environment variables
    load_dotenv()
    sirene_api_key = os.getenv('SIRENE_API_KEY')
    sirene_api_secret = os.getenv('SIRENE_API_SECRET')

    # Reset temp files from last run
    open('data/debug.log', 'w').close()

    #################
    #   VARIABLES   #
    #################
    city_queried = city_name
    city_insee_code = city_code
    prevent_update_insee = prevent_update_insee

    today_month_year = datetime.date.today().strftime("%Y-%m")
    now_datetime = datetime.datetime.now().strftime('%Y-%m-%d %H:%M')

    distance_to_poi = 30  # meters
    dict_osm_pois_tags = {
        "shop": {
            "whitelist": [],
            "blacklist": ["kiosk", "vacant"]
        },
        "amenity": {
            "whitelist": ["pub", "bar", "cafe", "restaurant", "fast_food", "nightclub", "bank", "ice_cream",
                          "car_rental", "stripclub", "theatre"],
            "blacklist": ["public_bookcase", 'public_building']
        }
    }

    #################
    #   MAIN CODE   #
    #################
    # Check if the 'SIRENE_API_KEY' environment variable is set
    if sirene_api_key is None or sirene_api_secret is None:
        print("Environment variables SIRENE_API_KEY/SIRENE_API_SECRET have not been set.")
        print("This script will not gather data from Sirene API and shop name matching will be less effective.")
        response = input(f"Do you want to continue anyway? (y/n): ").strip().lower()
        if response != 'y':
            print("Exiting script - Before running script again, please use :")
            print("export SIRENE_API_KEY=VOTRE_CLE_API")
            print("export SIRENE_API_SECRET = VOTRE_SECRET_API")
            exit(1)

    if city_queried:
        try:
            city_insee_code = get_insee_code_from_city_name(city_queried)
        except IndexError:
            sys.exit(f"Error: No Insee City Code found for {city_queried}, please check city name again "
                     f"in data/v_commune_2023.csv.")
        except Exception as e:
            sys.exit(f"Error: {str(e)}")
    elif city_insee_code:
        city_queried = get_city_name_from_insee_code(city_insee_code)

    final_sirene_file = f'data/sirene_checked-{city_insee_code}.json'
    csvfile_sirene = f'data/working_{city_insee_code}-{today_month_year}.csv'

    # Get latest Sirene data and extract specific city
    print(f"Pre-Processing {city_queried}")
    if not prevent_update_insee:
        download_unzip_last_version_sirene_csv()
        extract_city_businesses_from_sirene(city_insee_code, 9)

    poi_count = get_count_total_pois_city(city_queried, dict_osm_pois_tags)

    print(f"Collating {city_queried} data")
    print(f" |---> Getting latest OSM data (please wait)", end="", flush=True)

    df = get_df_pois_missing_address(city_queried, dict_osm_pois_tags, distance_to_poi)

    # Before moving forward, check if any business is incomplete
    if 'name' not in df.columns:
        print(f"\n       No incomplete business found in {city_queried}")
        print("....Exiting the script")
        sys.exit(0)  # Exit the script with a zero status to indicate a clean exit

    df_pois_with_name = df[df['name'] != ''].reset_index(drop=True)

    print(f"...Done")
    print(f" |---> Matching OSM with Sirene data ({len(df_pois_with_name)} POIs to check)")

    sirened_df = get_all_good_sirene_matches(city_queried, df_pois_with_name, csvfile_sirene)
    sirened_df.to_json(f'{final_sirene_file}', orient='index', indent=2)

    print(f'\r |---> Progress: 100%')

    # Gathering some stats
    # total_items = len(sirened_df)
    pois_with_one_sirene_match = sirened_df[sirened_df['sirene'].apply(lambda x: len(x[0]) if x is not None and
                                                            isinstance(x, list) and len(x) > 0 else 0) == 1].shape[0]
    pois_with_many_sirene_match = sirened_df[sirened_df['sirene'].apply(lambda x: len(x[0]) if x is not None and
                                                            isinstance(x, list) and len(x) > 0 else 0) > 1].shape[0]
    pois_with_no_sirene_match = sirened_df[sirened_df['sirene'].apply(lambda x: x is None or len(x) == 0)].shape[0]

    dict_pois_matching_stats = {
                        "date": now_datetime.split(' ')[0],
                        "time": now_datetime.split(' ')[1],
                        "city": city_queried,
                        "insee_code": city_insee_code,
                        "total_pois": int(poi_count['named']) + int(poi_count['unnamed']),
                        "total_pois_without_address": len(df),
                        "pois_without_name": int(poi_count['unnamed']),
                        "pois_with_no_sirene_match": pois_with_no_sirene_match,
                        "pois_with_many_sirene_match": pois_with_many_sirene_match,
                        "pois_with_one_sirene_match": pois_with_one_sirene_match,
                        "%_estimated_matching_rate": round(100*(pois_with_many_sirene_match +
                                                                pois_with_one_sirene_match) /
                                                   (len(df) - int(poi_count['unnamed'])), 1)}

    print(f"Creating {city_queried} webpage")
    print(f" |---> Creating frontend html", end="", flush=True)

    process_json_for_frontend(city_queried, final_sirene_file)

    city_lowered = city_queried.lower().replace(" ", "-")
    city_recent_stats_filename = f'website/ville/{city_lowered}/json/{city_lowered}_last_stats.json'

    with open(city_recent_stats_filename, 'w', encoding='utf-8') as f:
        json.dump(dict_pois_matching_stats, f, ensure_ascii=False, indent=2)

    print(f"...Done")
    print(f" |---> Downloading OSM tiles", end="", flush=True)

    process_osm_tiles(city_lowered, final_sirene_file)
    create_city_frontend(city_queried, dict_osm_pois_tags)
    build_dashboard_frontend()

    print(f"...Done")

    logger.info("\n###########################################################################")
    logger.info("######################## Matching Script POI Stats ########################")
    logger.info("###########################################################################")
    for key, value in dict_pois_matching_stats.items():
        logger.info(f"{key}: {value}")


if __name__ == '__main__':

    parser = argparse.ArgumentParser(description='Process cities for Sirene matching')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('-n', '--name', type=str, help='the name of the city')
    group.add_argument('-c', '--code', type=validate_city_insee_code, help='the city code (5-digit)')
    parser.add_argument('--prevent-update-insee', action='store_true', help='prevent update of INSEE data')
    args = parser.parse_args()

    run_complete_shops(city_name=args.name, city_code=args.code, prevent_update_insee=args.prevent_update_insee)
