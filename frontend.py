from collections import OrderedDict
import urllib.parse
import json
import os
from datetime import datetime


# Function to remove duplicates and maintain the order
def remove_duplicates(input_string):
    return ' '.join(OrderedDict((w, w) for w in input_string.split()).keys())


# Create a function to get the length of sirene
def get_length(item):
    if item['sirene'] is None:
        return 0
    else:
        # Count the number of key-value pairs in each dictionary
        return sum(len(business) for business in item['sirene'])


# Sort how the results are displayed on webpage (1st by least number of choices - 2nd by whether housetag is missing
def get_order(xitem):
    length = get_length(xitem)
    length_order = (length // 2) + 1
    addrtag_set_housetag_empty = bool(xitem.get('addrtag')) and not xitem.get('housetag')
    siret_set = bool(xitem.get('siret'))
    return length_order, addrtag_set_housetag_empty, siret_set


def process_json_for_frontend(city_queried, json_file):
    # Load your json
    with open(json_file) as f:
        data = json.load(f)

    # Filter out entries where sirene is null or empty
    filtered_data = [item for item in data.values() if get_length(item) > 0]

    # Remove duplicates from 'name' field in 'sirene'
    for item in filtered_data:
        for sirene in item['sirene']:
            for key in sirene:
                if 'name' in sirene[key]:
                    sirene[key]['name'] = remove_duplicates(sirene[key]['name'])

    # Sort your data
    sorted_data = sorted(filtered_data, key=get_order)

    # If the directory does not exist, create it
    directory_path = os.path.join('website', 'ville', city_queried.lower().replace(" ", "-"), 'json')
    if not os.path.exists(directory_path):
        os.makedirs(directory_path)

    # Write the sorted data to a new json file
    with open(os.path.join(directory_path, f'{city_queried.lower().replace(" ", "-")}_sirene_matches.json'), 'w') as f:
        json.dump(sorted_data, f, indent=2)


# Gather all city names in folder ville as well as their stats
def get_all_cities_processed():
    # Define the base directory
    script_dir = os.path.dirname(os.path.abspath(__file__))
    base_dir = os.path.join(script_dir, 'website/ville')

    # Prepare data structure for cities stats
    cities_present = []

    # Traverse the base directory to get city folders
    for city in os.listdir(base_dir):
        city_dir = os.path.join(base_dir, city)
        if os.path.isdir(city_dir):
            # Check for the json folder inside the city folder
            json_dir = os.path.join(city_dir, 'json')
            if os.path.isdir(json_dir):
                json_file = f"{city}_last_stats.json"
                json_path = os.path.join(json_dir, json_file)
                # Check if the JSON file exists and read data
                if os.path.isfile(json_path):
                    with open(json_path, 'r') as file:
                        data = json.load(file)
                        # Extract the required information
                        stats = {
                            'city_url': city,
                            'city': data.get('city', ''),
                            'insee_code': data.get('insee_code', ''),
                            'date': data.get('date', ''),
                            'time': data.get('time', ''),
                            'total_pois': data.get('total_pois', 0),
                            'pois_without_name': data.get('pois_without_name', 0),
                            'total_pois_without_address': data.get('total_pois_without_address', 0)
                        }
                        cities_present.append(stats)

    # Sort the cities_stats list by the 'city' key in each dictionary
    cities_present.sort(key=lambda x: x['city'].lower())

    return cities_present


def create_bulma_theme(city_queried):
    cities_stats = get_all_cities_processed()

    html_content = f"""
    <!DOCTYPE html>
    <html lang="fr" class="has-aside-left has-aside-mobile-transition has-navbar-fixed-top has-aside-expanded">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Dashboard - {city_queried}</title>
      <link rel="icon" type="image/svg+xml" href="../../media/completetescommerces.svg">
      <link rel="stylesheet" href="../../css/bulma.min.css">
      <link rel="stylesheet" href="../../css/watchmen.css">
    </head>
    <body>
    <div id="app">
      <!-- Navigation -->
      <nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
          <!-- Website Title -->
          <div class="navbar-item">
            <img src="../../media/completetescommerces.svg" alt="Complète Tes Commerces Logo" 
            style="height: 40px; width: auto; margin-right: 10px;">
            <h1 class="title is-size-3 is-size-5-mobile" style="color: black;">Complète Tes Commerces</h1>
          </div>
        </div>

        <!-- Right side of the navbar -->
        <div class="navbar-menu">
          <div class="navbar-end">
            <!-- Theme Toggle Button -->
            <div class="navbar-item">
              <button id="toggle-border-color" onclick="toggleTheme()" 
              style="background: none; border: none; cursor: pointer;">
                <img src="../../media/theme-light-dark.svg" 
                style="width: 70px; height: auto;" class="toggle-icon" alt="Dark Mode Icon">
              </button>
            </div>
          </div>
        </div>
      </nav>

    <!-- Sidebar -->
    <aside class="aside is-placed-left is-expanded flex-container">
      <div class="aside-tools">
        <div class="aside-tools-label">
          <span class="is-size-4"><b>Menu</b></span>
        </div>
      </div>
      <!-- Sidebar Content -->
      <div class="menu is-menu-main menu-section scrollable-menu">
        <!-- Existing menu items -->
        <p class="menu-label">Villes</p>
        <ul class="menu-list">"""

    for city_stat in cities_stats:
        city_name_slug = city_stat['city'].replace(" ", "-").lower()
        city_url = f"/ville/{city_name_slug}/index.html"
        html_content += f"""
              <li>
                <a href={city_url} class="sidebar-text">
                  <span class="menu-item-label">{city_stat['city']}</span>
                </a>
              </li>"""

    html_content += f"""
          </ul>
          </div>
          <!-- Bottom Links -->
          <div class="bottom-links bottom-links-section">
            <a href="https://gitlab.com/meryl_street/complete-tes-commerces" target="_blank" class="sidebar-text">
              <span>Repo GitLab</span>
            </a>
            <br>
            <br>
            <p class="sidebar-text" style="color: #666f81;">
              <span>Données OSM :</span>
            </p>
            <a href="https://opendatacommons.org/licenses/odbl/" target="_blank" class="sidebar-text">
              <span>Licence ODbL</span>
            </a>
            <p class="sidebar-text" style="color: #666f81;">
              <span>Données Sirene INSEE :</span>
            </p>
            <a href="https://www.etalab.gouv.fr/licence-ouverte-open-licence/" target="_blank" class="sidebar-text">
              <span>Licence LO/OL</span>
            </a>
            <br>
          </div>
        </aside>"""

    return html_content


# Create each city index.html
def create_city_frontend(city_queried, tags_dictionnary):

    city_lowered = city_queried.lower().replace(" ", "-")

    tags_query_string = "(\n"
    for poi_tag, value in tags_dictionnary.items():
        whitelist = "|".join(value["whitelist"])
        blacklist = "|".join(value["blacklist"])

        whitelist_string = f'["{poi_tag}"~"{whitelist}"]' if whitelist else ''
        blacklist_string = f'["{poi_tag}"!~"{blacklist}"]' if blacklist else ''

        tags_query_string += (f'nw["{poi_tag}"]{whitelist_string}{blacklist_string}[!"name"]["name:signed"!~"no"]'
                              f'(area.france)(area.ville);\n')

    query_pois_without_name = f'''
[out:json];
area["admin_level"="2"][name="France"]->.france;
area["admin_level"="8"][name="{city_queried}"]->.ville;
{tags_query_string}
);
out body;
>	;
out skel qt;
        '''

    data_encoded = urllib.parse.quote(query_pois_without_name)
    overpass_url_pois_without_name = f"http://overpass-turbo.eu/?Q={data_encoded}&R"

    html_content = create_bulma_theme(city_queried)

    html_content += f"""
<section class="section is-title-bar py-4">
  <div class="level">
    <!-- Logo and home link -->
    <div class="level-left">
      <div class="level-item">
        <a href="https://www.complete-tes-commerces.fr">
          <img src="../../media/homepage.svg" style="width: 25px; height: auto;" 
          class="homepage-icon" alt="Complete Tes Commerces Logo">
        </a>
      </div>
      <div class="level-item">
        <h1 class="title is-size-4" style="color: black;">
          / {city_queried}
        </h1>
      </div>
      <!-- Tab navigation -->
      <div class="level-item">
        <div class="tabs is-boxed">
          <ul>
              <li class="is-active"><a href="index.html">Ajout SIRET/Adresse</a></li>
              <li><a href="siret_clos.html">SIRETs Clôturés</a></li>
              <li><a href="commerces_vides.html">Commerces Désaffectés</a></li>          
          </ul>
        </div>
      </div>
    </div>
  </div>
</section>
<section class="section is-main-section has-less-padding">
  <div class="columns">
    <div class="column is-half" style="min-height: 394px; justify-content: flex-start; 
                        flex-direction: column; display: flex;">
      <!-- Card for the list items -->
      <div class="card has-less-padding has-less-margin" style="display: flex; flex-direction: column; 
                        justify-content: space-between; height: 100%; border: 0px">
          <div class="columns is-multiline stats-info" style="margin: auto 0;">
            <!-- Boxes will be dynamically inserted here -->
            <div id="matching-rate-sentence" class="column is-one-fourth" style="padding: 0.5rem; 
                        border: 0.2px solid #dbdbdb;margin: 0.5rem;">
              <!-- Matching rate sentence box content will be inserted here -->
            </div>
            <div id="pois-without-name-sentence" class="column is-one-fourth" 
                        data-url="{overpass_url_pois_without_name}" style="padding: 0.5rem; 
                        border: 0.2px solid #dbdbdb; margin: 0.5rem;">
              <!-- POIs without name sentence box content will be inserted here -->
            </div>
            <div id="script-last-run-datetime" class="column is-one-fourth" style="padding: 0.5rem; 
                        border: 0.2px solid #dbdbdb; margin: 0.5rem;">
              <!-- Last run datetime box content will be inserted here -->
            </div>
            <div id="add hashtag" class="column is-two-fifth" style="padding: 0.5rem; 
                        border: 0.2px solid #dbdbdb;margin: 0.5rem;">
                  <div class="has-text-centered">
                    <span class="has-text-weight-bold is-size-5">#completetescommerces</span>
                    <div>À ajouter pour suivre les contributions 
                    <a href="https://osmcha.org/?aoi=e94937e6-39f1-402c-a2ed-f2d2ec4fd243" target="_blank" 
                        rel="noopener noreferrer">
                        <img src="../../media/ext-link-black.svg" alt="External Link Icon" class="ext-link"
                        style="width: 16px; height: 16px;" />
                    </a>
                    </div>
                  </div>
            </div>
          </div>
      </div>
        <!-- Card for the progress bar -->
        <div class="card-content no-side-padding has-less-padding">
          <div id="stats-bar-container" class="stats-bar">
            <!-- Progress bar content will be dynamically inserted here -->
          </div>
        </div>
      </div>
    <!-- Right half for canvas -->
    <div class="column is-half">    
        <div class="card has-less-margin">
          <header class="card-header">
            <p class="card-header-title">
              <img src="../../media/chart.svg" class="theme-icon" 
              style="width: 2%; height: auto; margin-right: 10px;"><img>
              Évolution des Tags de Commerces
            </p>
          </header>
          <div class="card-content has-less-padding">
            <div class="chart-area">
              <div style="height: 100%;">
                <div class="chartjs-size-monitor">
                  <div class="chartjs-size-monitor-expand">
                    <div></div>
                  </div>
                  <div class="chartjs-size-monitor-shrink">
                    <div></div>
                  </div>
                </div>
                <canvas id="big-line-chart" class="chartjs-render-monitor" 
                style="display: block; min-height: 300px;"></canvas>
              </div>
            </div>
          </div>
        </div>    
    </div>
  </div>
    <div class="card">
      <header class="card-header has-margin-bottom-1">
        <div class="card-header-title message-header warning-box" style="color: white">
          Attention
        <button class="delete message-header" aria-label="close" 
        onclick="this.parentElement.parentElement.parentElement.style.display='none';"></button>
        </div>
      </header>
      <div class="card-content has-text-eed68e has-less-padding">
        <div class="content">
          Pour chaque commerce sans adresse dans OSM, les tuiles ci-dessous présentent un ou plusieurs commerces  
          similaires retrouvés dans la base de données ouverte <a 
          href="https://www.data.gouv.fr/fr/datasets/base-sirene-des-entreprises-et-de-leurs-etablissements-siren-siret/">
          Sirene</a> de l'INSEE.
          <br>Les choix présentés sont <b>sans garantie</b>, une vérification et du bon sens sont donc nécessaires lors
           de tout ajout/modification dans OSM.
        </div>
      </div>
    </div>
<div class="columns is-vcentered is-mobile">
    <div class="column has-text-centered is-3">
        <label class="checkbox">
            <input type="checkbox" class="siret-checkbox" id="toggleSiretTiles">
            <span class="is-size-7-mobile">Filtrage des tags ref:FR:SIRET</span>
        </label>
    </div>
    <div class="column has-text-centered is-half">
        <h1 class="title">
            Correspondances Base Sirene
        </h1>
    </div>
    <div class="column is-3">
        <!-- Question mark icon with tooltip -->
        <div class="help-icon-container" style="display: flex; justify-content: space-between; 
                                                align-items: center; padding-right: 21px;">
            <div style="flex-grow: 1; text-align: center;">
                <!-- The osmLink will be inserted here in the center -->
            </div>
            <div class="tooltip" style="flex-shrink: 0;">
                <img src="../../media/question-mark.svg" alt="Help" class="question-mark" style="vertical-align: middle; 
                                                transform: scale(1.5); transform-origin: center;">
                <span class="tooltiptext has-text-left">
                    - Pour éditer avec JOSM, il faut d'abord sélectionner une des propositions de la tuile.<br><br>
                    - Cliquer sur la minicarte ouvre la page du commerce sur osm.org.<br><br>
                    - Les tuiles sont présentes lorsque les tags d'adresse ne sont pas complets (par exemple 
                    sans tag contact:housenumber).<br><br>
                    - Un filtrage des tuiles sans adresse mais avec un numéro SIRET est possible sur le côté gauche.
                </span>
            </div>
        </div>
    </div>
</div>
    <div class="card">
      <div class="card-content">
        <div class="tile-container">
        </div>
      </div>
    </div>
  </section>
  <script>
  window.jsonFilePath = `../../ville/{city_lowered}/json/{city_lowered}_sirene_matches.json`;
  window.lastStatsPath = `../../ville/{city_lowered}/json/{city_lowered}_last_stats.json`;
  window.dailyStatsPath = `../../ville/{city_lowered}/json/{city_lowered}_dailystats.json`;
  </script>
  <script type="text/javascript" src="../../js/watchmen.js"></script>
  <script type="text/javascript" src="../../js/chart.js"></script>
  <script type="text/javascript" src="../../js/bulma.min.js"></script>
  <script type="text/javascript" src="../../js/toggle_theme.js"></script>
</body>
</html>
    """

    with open(f'website/ville/{city_lowered}/index.html', "w") as file:
        file.write(html_content)


dicttags = {
    "shop": {
        "whitelist": [],
        "blacklist": ["kiosk", "vacant"]
    },
    "amenity": {
        "whitelist": ["pub", "bar", "cafe", "restaurant", "fast_food", "nightclub", "bank", "ice_cream", "car_rental",
                      "stripclub", "theatre"],
        "blacklist": ["public_bookcase", 'public_building']
    }
}


def build_dashboard_frontend():

    cities_stats = get_all_cities_processed()

    # Get current date and time for the footer
    current_datetime = datetime.now()
    formatted_date = current_datetime.strftime('%d/%m/%Y')
    formatted_time = current_datetime.strftime('%H:%M')

    # Start building the HTML content
    html_content = f"""<!DOCTYPE html>
    <html lang="fr" class="has-aside-left has-aside-mobile-transition has-navbar-fixed-top has-aside-expanded">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <title>Complète Tes Commerces</title>
      <link rel="icon" type="image/svg+xml" href="media/completetescommerces.svg">
      <link rel="stylesheet" href="css/bulma.min.css">
      <link rel="stylesheet" href="css/watchmen.css">
    </head>
    <body>
    <div id="app">
      <!-- Navigation -->
      <nav class="navbar is-fixed-top" role="navigation" aria-label="main navigation">
        <div class="navbar-brand">
          <!-- Website Title -->
          <div class="navbar-item">
            <img src="media/completetescommerces.svg" alt="Complète Tes Commerces Logo" style="height: 40px; 
            width: auto; margin-right: 10px;">
            <h1 class="title is-size-3 is-size-5-mobile" style="color: black;">Complète Tes Commerces</h1>
          </div>
          <!-- Burger button for sidebar -->
          <a role="button" class="navbar-burger" data-target="navbarMenu" aria-label="menu" aria-expanded="false">
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>
        
        <!-- Navbar menu (hidden on desktop, visible on mobile) -->
        <div id="navbarMenu" class="navbar-menu greenish is-hidden-desktop">
          <div class="navbar-start">
            <a href="https://lite.framacalc.org/cwlmhq6pt2-a4vl" target="_blank" class="navbar-item">
              <span>> Ajouter une Ville</span>
            </a>
            <a href="https://gitlab.com/meryl_street/complete-tes-commerces" target="_blank" class="navbar-item">
              <span>Repo GitLab</span>
            </a>
            <a href="https://opendatacommons.org/licenses/odbl/" target="_blank" class="navbar-item">
              <span>Données OSM : Licence ODbL</span>
            </a>
            <a href="https://www.etalab.gouv.fr/licence-ouverte-open-licence/" target="_blank" class="navbar-item">
              <span>Données Sirene INSEE : Licence LO/OL</span>
            </a>
          </div>
        </div>
      </nav>
      
    <!-- Sidebar -->
    <aside class="aside is-placed-left is-expanded flex-container">
      <div class="aside-tools">
        <div class="aside-tools-label">
          <span class="is-size-4"><b>Menu</b></span>
        </div>
      </div>
      <!-- Sidebar Content -->
      <div class="menu is-menu-main menu-section scrollable-menu">
        <!-- Existing menu items -->
        <p class="menu-label">Villes</p>
        <ul class="menu-list">"""

    for city_stat in cities_stats:
        city_name_slug = city_stat['city'].replace(" ", "-").lower()
        city_url = f"/ville/{city_name_slug}/index.html"
        html_content += f"""
          <li>
            <a href={city_url} class="sidebar-text">
              <span class="menu-item-label">{city_stat['city']}</span>
            </a>
          </li>"""

    html_content += f"""
        </ul>
      </div>
      <!-- Bottom Links -->
      <div class="bottom-links bottom-links-section">
        <a href="https://lite.framacalc.org/cwlmhq6pt2-a4vl" target="_blank" class="sidebar-text" 
                        style="color: #666f81;">
          <span class="menu-item-label">> Ajouter une Ville</span>
        </a>
        <br>
        <a href="https://gitlab.com/meryl_street/complete-tes-commerces" target="_blank" class="sidebar-text">
          <span>Repo GitLab</span>
        </a>
        <br>
        <p class="sidebar-text" style="color: #666f81;">
          <span>Données OSM :</span>
        </p>
        <a href="https://opendatacommons.org/licenses/odbl/" target="_blank" class="sidebar-text">
          <span>Licence ODbL</span>
        </a>
        <p class="sidebar-text" style="color: #666f81;">
          <span>Données Sirene INSEE :</span>
        </p>
        <a href="https://www.etalab.gouv.fr/licence-ouverte-open-licence/" target="_blank" class="sidebar-text">
          <span>Licence LO/OL</span>
        </a>
        <br>
      </div>
    </aside>
    <div class="section pt-6">
        <p>Cet outil se concentre sur l'<strong>exhaustivité des tags pour les commerces déjà présents</strong> dans 
        OpenStreetMap. L'ajout de nouveaux commerces à d'autres initiatives tels 
        qu'<a href="https://osmose.openstreetmap.fr/fr/map" target="_blank">Osmose</a> et 
        <a href="http://commerces.openstreetmap.fr" target="_blank">Banco</a>.</p>
        <br>
        <p>Il y a trois fonctions principales :</p>
        <br>
        <ul class="custom-ul">
          <li><strong>Intégration des tags adresse/SIRET</strong> : rapprochement des commerces, disposant d'un 
          tag name mais sans adresse, avec la base Sirene.</li>
          <li><strong>Actualisation des SIRET non actifs</strong> : propositions de nouveaux SIRETs pour les commerces 
          qui sont signalés comme fermés dans la base Sirene.</li>
          <li><strong>Identification des commerces désaffectés</strong> : mise en évidence des commerces marqués 
          comme disused ou vacant depuis > 6 mois.</li>
        </ul>
        <br>
        <p>Nous nous efforçons de fournir des informations ciblées et d'éviter les faux positifs mais nous ne pouvons 
        pas les éliminer complètement. Nous encourageons donc tous les utilisateurs à vérifier le sens des propositions 
        avant de les ajouter à OSM.</p>
    </div>
    <section class="section pt-4">
        <div class="container mt-5">
            <table id="statsTable" class="table is-bordered is-striped is-narrow is-hoverable is-fullwidth ">
                <thead>
                    <tr>
                        <th class="has-text-centered">Ville</th>
                        <th class="has-text-centered is-hidden-mobile">Code Insee</th>                            
                        <th class="has-text-centered is-hidden-mobile">Nombre de Commerces</th>
                        <th class="has-text-centered is-hidden-mobile">Nom Manquant</th>
                        <th class="has-text-centered is-hidden-mobile">Adresse Manquante</th>
                        <th class="has-text-centered" data-sort="update">Mis à Jour</th>
                    </tr>
                </thead>
                <tbody>"""

    # Add rows for each city with Bulma classes
    for city_stat in cities_stats:
        city_name_slug = city_stat['city'].replace(" ", "-").lower()
        city_url = f"/ville/{city_name_slug}/index.html"
        html_content += f"""
    <tr>
        <td class="has-text-centered"><a href="{city_url}" target="_blank">{city_stat['city']}</a></td>
        <td class="has-text-centered is-hidden-mobile">{city_stat['insee_code']}</td>
        <td class="has-text-centered is-hidden-mobile">{city_stat['total_pois']}</td>
        <td class="has-text-centered is-hidden-mobile">{city_stat['pois_without_name']} ({round(100 * city_stat['pois_without_name'] / 
                                                     city_stat['total_pois'], 1)}%)</td>
        <td class="has-text-centered is-hidden-mobile">{city_stat['total_pois_without_address']} 
        ({round(100 * city_stat['total_pois_without_address'] / city_stat['total_pois'], 1)}%)</td>
        <td class="has-text-centered">{city_stat['date']}</td>
    </tr>"""

    # Finish the HTML content and add the footer with Bulma classes
    html_content += f"""
                    </tbody>
                </table>
            </div>
        </section>
    </div>
        <footer class="footer">
            <div class="content has-text-centered">
                <p>
                    Compilé le {formatted_date} à {formatted_time}
                </p>
            </div>
        </footer>
    </body>
    <script type="text/javascript" src="js/watchmen.js"></script>
    </html>"""

    # Write the HTML content to index.html in the website folder
    with open('website/index.html', 'w') as html_file:
        html_file.write(html_content)

    # Create redirect page for ville/
    html_redirect_content = """<!DOCTYPE html>
    <html>
        <head>
            <meta http-equiv="refresh" content="0; URL='https://complete-tes-commerces.fr/'" />
            <title>Redirection</title>
        </head>
        <body>
            <h1>Redirection</h1>
            <p>Si vous n'etes pas redirigé vers la page d'accueil d'ici 5 secondes, 
            <a href="https://complete-tes-commerces.fr/">cliquez ici</a>.
            </p>
        </body>
    </html>
    """

    with open('website/ville/index.html', 'w') as html_file:
        html_file.write(html_redirect_content)
